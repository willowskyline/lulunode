

lln_SyncPin = {}
lln_SyncPin.Metatable =
{__index = lln_SyncPin}

lln_SyncPin.Defaults =
{	x = RSkyl.Required
,	y = RSkyl.Required
}

lln_SyncPin.Mapping =
{	x = "x"
,	y = "y"
}

function lln_SyncPin.New (a)
	return	lln_MetaConstructor ( a
		,	lln_SyncPin)
end

function lln_SyncPin:Draw (w)
	local x =  w*self.x + 10
	local y = 20*self.y + 10
	
	RSkyl.SetColor ("White")
	RSkyl.TriangleFan {
		x-7 ,	y	,
		x   ,	y-7 ,
		x	,	y-4	,
		x-4	,	y	,
		x	,	y+4	,
		x	,	y+7	}
	
	RSkyl.TriangleFan {
		x+7 ,	y	,
		x   ,	y-7 ,
		x	,	y-4	,
		x+4	,	y	,
		x	,	y+4	,
		x	,	y+7	}
end

function lln_SyncPin:Detect (x, y, w)
	return math.abs(x -  w*self.x - 10)
	     + math.abs(y - 20*self.y - 10) <= 7
end
