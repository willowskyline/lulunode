Image = {}
Image.Metatable =
{__index = Image}

Image.Defaults =
{	image   = false

,	rgba    = {1,1,1,1}
,	red     = false
,	green   = false
,	blue    = false
,	alpha   = false
}

function Image:Class_build ()
	return self:Build_rgba ()
end

function Image:Class_draw (d_x, d_y, w, h, a)
	if not self.image then return false end

	local image_width, image_height = self.image:getDimensions ()
	local scale_x = w / image_width
	local scale_y = h / image_height

	self:Use_rgba ()
	love.graphics.draw (self.image, d_x, d_y, 0, scale_x, scale_y)
	return true
end

return Image
