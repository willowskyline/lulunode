local VDivider = {}
VDivider.Metatable =
{__index = VDivider}

VDivider.Defaults =
{	Dispute    = {}
,	Proportion = {}
}

function VDivider:Class_build ()
	local dispute_sum = 0
	local proportion_sum = 0

	--	collect sums
	for i = 1, #self do
		local dispute = self.Dispute[i] or 0
		dispute_sum = dispute_sum + dispute
		self.Dispute [i] = dispute
		local proportion = self.Proportion[i] or 1
		proportion_sum = proportion_sum + proportion
		self.Proportion [i] = proportion
	end
	self.Dispute.sum = dispute_sum
	--	normalise
	for i = 1, #self do
		self.Proportion [i] = self.Proportion[i]/proportion_sum
	end

	return self
end

function VDivider:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local spare = h - a*self.Dispute.sum
	if spare <= 0 then return false end

	local References = {}
	local off_y = 0
	for i = 1, #self do
		local child_h = a*self.Dispute[i] + spare*self.Proportion[i]
		References [i] = self[i]:Reference (
			p_x, p_y + off_y,
			d_x, d_y + off_y,
			w, child_h, a)
		off_y = off_y + child_h
	end
	return References
end

return VDivider
