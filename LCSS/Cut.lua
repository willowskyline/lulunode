local Cut = {}
Cut.Metatable =
{__index = Cut}

Cut.Defaults =
{	rgba  = {1,1,1,1}
,	red   = false
,	green = false
,	blue  = false
,	alpha = false

,	cut = 9
}

function Cut:Class_build ()
	return self:Build_rgba ()
end

function Cut:Class_draw (d_x, d_y, w, h, a)
	local e_x = d_x + w
	local e_y = d_y + h
	local c = a*self.cut

	if c > w or c > h then return false end

	self:Use_rgba ()
	love.graphics.polygon ('fill'
	, d_x   , e_y
	, e_x   , e_y
	, e_x   , d_y+c
	, e_x-c , d_y
	, d_x   , d_y
	)
	return true
end

return Cut
