local Absolute = {}
Absolute.Metatable =
{__index = Absolute}

Absolute.Defaults =
{	width   = 32
,	height  = 32
,	align_h = 0.5
,	align_v = 0.5
}

function Absolute:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local off_x = (w - a*self.width )*self.align_h
	local off_y = (h - a*self.height)*self.align_v

	local list = {}
	for i = 1, #self do
		list [i] = self[i]
		:Reference (
			p_x + off_x, p_y + off_y,
			d_x + off_x, d_y + off_y,
			a*self.width, a*self.height, a)
	end
	return list
end

return Absolute
