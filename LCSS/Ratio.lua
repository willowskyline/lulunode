local Ratio = {}
Ratio.Metatable =
{__index = Ratio}

Ratio.Defaults =
{	ratio   = 1 --w/h
,	align_h = 0.5
,	align_v = 0.5
}

function Ratio:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local in_ratio = w/h
	local child_w
	local child_h
	--	horizontally limited
	if in_ratio < self.ratio then
		child_w = w
		child_h = w/self.ratio
	else
		child_h = h
		child_w = h*self.ratio
	end

	local off_x = (w - child_w)*self.align_h
	local off_y = (h - child_h)*self.align_v

	local list = {}
	for i = 1, #self do
		list [i] = self[i]
		:Reference (
			p_x + off_x, p_y + off_y,
			d_x + off_x, d_x + off_y,
			child_w, child_h, a)
	end
	return list
end

return Ratio
