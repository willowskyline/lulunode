local HDivider = {}
HDivider.Metatable =
{__index = HDivider}

HDivider.Defaults =
{	Dispute    = {}
,	Proportion = {}
}

function HDivider:Class_build ()
	local dispute_sum = 0
	local proportion_sum = 0

	--	collect sums
	for i = 1, #self do
		local dispute = self.Dispute[i] or 0
		dispute_sum = dispute_sum + dispute
		self.Dispute [i] = dispute
		local proportion = self.Proportion[i] or 1
		proportion_sum = proportion_sum + proportion
		self.Proportion [i] = proportion
	end
	self.Dispute.sum = dispute_sum
	--	normalise
	for i = 1, #self do
		self.Proportion [i] = self.Proportion[i]/proportion_sum
	end

	return self
end

function HDivider:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local spare = w - a*self.Dispute.sum
	if spare <= 0 then return false end

	local References = {}
	local off_x = 0
	for i = 1, #self do
		local child_w = a*self.Dispute[i] + spare*self.Proportion[i]
		References [i] = self[i]:Reference (
			p_x + off_x, p_y,
			d_x + off_x, d_y,
			child_w, h, a)
		off_x = off_x + child_w
	end
	return References
end

return HDivider
