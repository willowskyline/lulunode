local HScroller = {}
HScroller.Metatable =
{__index = HScroller}

HScroller.Defaults =
{	uniform = 32
,	Dispute = {}
,	gap     = 0
,	offset  = 0
}

HScroller.use_canvas = true

function HScroller:Class_build ()
	local dispute_sum = 0
	for i = 1, #self do
		local dispute = self.Dispute[i] or self.uniform
		dispute_sum = dispute_sum + dispute
		self.Dispute [i] = dispute
	end
	self.Dispute.sum = dispute_sum
	return self
end

function HScroller:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local content_w = a*(self.Dispute.sum + (#self - 1)*self.gap)
	local content_overspill = math.max (content_w - w, 0)
	local off_x = -self.offset*content_overspill

	local References = {}
	for i = 1, #self do
		local child_w = a*self.Dispute[i]
		References [i] = self[i]:Reference (
			p_x + off_x, py,
			off_x, 0,
			child_w, h, a)
		off_x = off_x + child_w + a*self.gap
	end
	return References
end

return HScroller
