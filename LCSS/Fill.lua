local Fill = {}
Fill.Metatable =
{__index = Fill}

Fill.Defaults =
{	rgba  = {1,1,1,1}
,	red   = false
,	green = false
,	blue  = false
,	alpha = false
}

function Fill:Class_build ()
	return self:Build_rgba ()
end

function Fill:Class_draw (d_x, d_y, w, h, a)
	self:Use_rgba ()
	love.graphics.rectangle ('fill', d_x, d_y, w, h)
	return true
end

return Fill
