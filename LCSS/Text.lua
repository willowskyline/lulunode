local Text = {}
Text.Metatable =
{__index = Text}

Text.Defaults =
{	text = ""
,	align = "left"

,	rgba  = {1,1,1,1}
,	red   = false
,	green = false
,	blue  = false
,	alpha = false
}

function Text:Class_build ()
	return self:Build_rgba ()
end

function Text:Class_draw (d_x, d_y, w, h, a)
	self:Use_rgba ()
	love.graphics.printf (self.text, d_x, d_y, w, self.align)
	return true
end

return Text
