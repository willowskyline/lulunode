local Cache = {}
Cache.Metatable =
{__index = Cache}

Cache.Defaults =
{	cache_x = 0
,	cache_y = 0
,	cache_width = 0
,	cache_height = 0
}

function Cache:Class_draw (pos_x, pos_y, width, height)
	self.cache_x = pos_x
	self.cache_y = pos_y
	self.cache_width  = width
	self.cache_height = height
	return true
end

return Cache
