local VScroller = {}
VScroller.Metatable =
{__index = VScroller}

VScroller.Defaults =
{	uniform = 32
,	Dispute = {}
,	gap     = 0
,	offset  = 0
}

VScroller.use_canvas = true

function VScroller:Class_build ()
	local dispute_sum = 0
	for i = 1, #self do
		local dispute = self.Dispute[i] or self.uniform
		dispute_sum = dispute_sum + dispute
		self.Dispute [i] = dispute
	end
	self.Dispute.sum = dispute_sum
	return self
end

function VScroller:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local content_h = a*(self.Dispute.sum + (#self - 1)*self.gap)
	local content_overspill = math.max (content_h - h, 0)
	local off_y = -self.offset*content_overspill

	local References = {}
	for i = 1, #self do
		local child_h = a*self.Dispute[i]
		References [i] = self[i]:Reference (
			p_x, p_y + off_y,
			off_x, 0,
			w, child_h, a)
		off_y = off_y + child_h + a*self.gap
	end
	return References
end

return VScroller
