--[[ RSkyl - Ranford's Skyline lua toolset ]]

RSkyl = {}

--	misc

--	a function that returns nil
function RSkyl.NilFunction ()
	return
end

--	unique table to denote a required field
RSkyl.Required = {}

--	math

function RSkyl.Lerp (a, b, t)
	return a + (b-a)*t
end

--	table

RSkyl.unpack = unpack or table.unpack

function RSkyl.IsIn (t, e)
	for k,v in pairs(t) do
		if v==e then return k end
	end
end

function RSkyl.Last (t)
	return t[#t]
end

function RSkyl.RecursivePrint (val, depth, tab, indent)
	print (indent..tostring(val))
	if type(val) == type {} and depth > 0 then
		indent = indent ..tab
		depth = depth - 1
		for k,c in pairs(val) do
			RSkyl.RecursivePrint (c, depth, tab, indent)
		end
	end
end

function RSkyl.TableDefault (tab, default)
	local res = {}
	for k,v in pairs(default) do
		if v == RSkyl.Required then
			RSkyl.TableRequire (tab, k)
		end
		res[k] = tab[k] or v
	end
	return res
end

function RSkyl.TableMap (tab, map)
	local res = {}
	for k,v in pairs(map) do
		res[v] = tab[k]
	end
	return res
end

function RSkyl.TableRequire (tab, field)
	assert (tab[field] ~= nil,
	"Required field '"..tostring(field).."' not found")
end

function RSkyl.TableSub (t, s, e)
	local l = #t
	s = s or 1
	e = e or l
	if s < 0 then s = l + s end
	if e < 0 then e = l + e end
	local r = {}
	for i=s,e do
		table.insert (r, t[i])
	end
	return r
end

--	takes any number of arguments and returns true if none of
--	them are false
function RSkyl.And (...)
	local a = {...}
	for i=1,#a do
		if not a[i] then return false end
	end
	return true
end

--	takes any number of arguments and returns true if any of
--	them are true
function RSkyl.Or (...)
	local a = {...}
	for i=1,#a do
		if a[i] then return true end
	end
	return false
end

--	love libs

--	sets the love2d drawing color via "rgba" or "name, alpha"
function RSkyl.SetColor (r,g,b,a)
	if type(r) == type"" then
		a = g or 1.0
		r,g,b = RSkyl.unpack (ColorTable[r])
	end
	love.graphics.setColor(r,g,b,a)
end

function RSkyl.TriangleFan (a)
	local ax = a[1]
	local ay = a[2]
	local bx = false
	local by = false
	local cx = a[3]
	local cy = a[4]
	
	for i=5,#a,2 do
		bx = cx
		by = cy
		cx = a[ i ]
		cy = a[i+1]
		love.graphics.polygon('fill', ax, ay, bx, by, cx, cy)
	end
end

--	lln

function lln_MetaConstructor (tab, class)
	return setmetatable(
		RSkyl.TableMap(
			RSkyl.TableDefault( tab
			, class.Defaults
		), class.Mapping
	), class.Metatable)
end
