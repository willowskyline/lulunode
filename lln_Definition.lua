require "lln_Types"
require "lln_Hooks"

require "lln_Grab"
require "lln_ValuePin"
require "lln_Visual"
require "lln_TextBox"
require "lln_InputBox"

lln_Definition = {}
lln_Definition.Metatable =
{__index = lln_Definition}

lln_Definition.Defaults =
{	w = RSkyl.Required
,	h = RSkyl.Required
,	H = lln_Hook.None
,	n = RSkyl.Required
,	s = false
}

lln_Definition.Mapping =
{	w = "width"
,	h = "height"
,	H = "hook"
,	n = "name"
,	s = "sync"
}

lln_Definition.List = {}

function lln_Definition.New (a)
	local new = lln_MetaConstructor ( a
			,	lln_Definition)
	new.Component =
	{	Grab	 = {}
	,	Visual	 = {}
	,	TextBox	 = {}
	,	PinIn	 = {}
	,	PinOut	 = {}
	,	InputBox = {}
	}
	
	lln_Definition.List [new.name] = new
	return new
end

function lln_Definition:Draw (w, io)
	local comp = self.Component
	for _,g in ipairs(comp.Grab) do
		g:Draw (w)
	end
	for _,v in ipairs(comp.Visual) do
		v:Draw (w)
	end
	for _,t in ipairs(comp.TextBox) do
		t:Draw (w, io[t])
	end
	for _,p in ipairs(comp.PinIn) do
		p:Draw (w, io[p].T)
	end
	for _,p in ipairs(comp.PinOut) do
		p:Draw (w, io[p])
	end
	for _,i in ipairs(comp.InputBox) do
		i:Draw (w, io[i])
	end
end

function lln_Definition:Detect (x,y,w)
	local comp = self.Component
	
	for _,p in ipairs(comp.PinIn) do
		if p:Detect (x,y,w) then
			return {comp=p, sub="in"}
		end
	end
	for _,p in ipairs(comp.PinOut) do
		if p:Detect (x,y,w) then
			return {comp=p, sub="out"}
		end
	end
	for _,i in ipairs(comp.InputBox) do
		if i:Detect (x,y,w) then
			return {comp=i, sub="box"}
		end
	end
	for _,g in ipairs(comp.Grab) do
		local res = g:Detect (x,y,w)
		if res then
			return {comp=g, sub=res}
		end
	end
	for _,v in ipairs(comp.Visual) do
		local res = v:Detect (x,y,w)
		if res then
			return {comp=v, sub="visual"}
		end
	end
end

function lln_Definition:GenerateReferences ()
	local ref = {}
	for _,t in ipairs (self.Component.TextBox) do
		ref[t] = t.text
	end
	for _,p in ipairs (self.Component.PinIn) do
		ref[p] = lln_InputReference.New (p.T)
	end
	for _,p in ipairs (self.Component.PinOut) do
		ref[p] = p.T
	end
	for _,i in ipairs (self.Component.InputBox) do
		ref[i] = i.text
	end
	return ref
end

function lln_Definition:GetByID (id)
	for _,t in ipairs (self.Component.TextBox) do
		if t.id == id then return t end
	end
	for _,p in ipairs (self.Component.PinIn) do
		if p.id == id then return p end
	end
	for _,p in ipairs (self.Component.PinOut) do
		if p.id == id then return p end
	end
	for _,i in ipairs (self.Component.InputBox) do
		if i.id == id then return i end
	end
end



function lln_Definition:AddGrab (a)
	table.insert ( self.Component.Grab
	,	lln_Grab.New(a))
	return self
end

function lln_Definition:AddVisual (a)
	table.insert ( self.Component.Visual
	,	lln_Visual.New(a))
	return self
end

function lln_Definition:AddTextBox(a)
	table.insert ( self.Component.TextBox
	,	lln_TextBox.New(a))
	return self
end

function lln_Definition:AddInPin (a)
	table.insert ( self.Component.PinIn
	,	lln_ValuePin.New(a))
	return self
end

function lln_Definition:AddOutPin (a)
	table.insert ( self.Component.PinOut
	,	lln_ValuePin.New(a))
	return self
end

function lln_Definition:AddInputBox (a)
	table.insert ( self.Component.InputBox
	,	lln_InputBox.New(a))
	return self
end



function lln_Definition:AddStandardBackground ()
	return self
		:AddVisual {x=0, y=0, h=self.height, b=2, a=0.75}
end

function lln_Definition:AddStandardGrab (t)
	return self
		:AddGrab	{}
		:AddTextBox	{x=0, y=0, r=10, t=t, a="center", i="_title"}
end

function lln_Definition:AddStandardPinIn (y, i, t, T)
	return self
		:AddVisual	{x=0, y=y}
		:AddInPin	{x=0, y=y, i=i, T=T}
		:AddTextBox	{x=0, y=y, l=5, t=t}
end

function lln_Definition:AddStandardPinOut (y, i, t, T)
	return self
		:AddVisual	{x=0, y=y}
		:AddOutPin	{x=1, y=y, i=i, T=T}
		:AddTextBox	{x=0, y=y, r=5, t=t, a="right"}
end

function lln_Definition:AddStandardSyncMark (y)
	return self
		:AddVisual  {x=0/3, y=y, w=1/3}
		:AddVisual  {x=1/3, y=y, w=1/3, I=self.sync}
		:AddVisual  {x=2/3, y=y, w=1/3}
end

lln_Definition.StandardDefaults =
{	w = 60
,	n = RSkyl.Required
,	s = false
}

--	a[i] = {i="",t="",T="",d=""}
--	r[i] = {i="",t="",T="",d=""}

function lln_Definition.NewStandard (name, title, args, rets, doc, sync)
	local h = #args + #rets + 1 + (sync and 1 or 0)
	local new = lln_Definition.New {n=name, w=60, h=h, s=sync}
		:AddStandardBackground	()
		:AddStandardGrab		(title)
	
	if sync then
		new:AddStandardSyncMark (h-1)
	end
	
	for i,a in ipairs(args) do
		new:AddStandardPinIn  (i  , a.i, a.t, a.T, a.d)
	end
	h = #args
	for i,r in ipairs(rets) do
		new:AddStandardPinOut (i+h, r.i, r.t, r.T, r.d)
	end
	
	return new
end



function lln_Definition:LinkHook (H)
	self.hook = H
end
