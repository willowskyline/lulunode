--[[ LCSS ]]

local LCSS = {}
LCSS.Metatable =
{__index = LCSS}

function LCSS.Table_clone_surface (Table)
	local New = {}
	for key, value in pairs (Table) do
		New [key] = value
	end
	return New
end

function LCSS.Table_default (Table, Default)
	for key, value in pairs (Default) do
		if Table [key] == nil then
			if type (value) == "table" then
				value = LCSS.Table_clone_surface (value)
			end
			Table [key] = value
		end
	end
	return Table
end

LCSS.Element_Reference = {}
LCSS.Element_Reference.Metatable =
{__index = LCSS.Element_Reference}

function LCSS.Element_Reference.New (Element, p_x, p_y, d_x, d_y, w, h, a)
	return setmetatable (
	{	Class = Element
	,	p_x = p_x
	,	p_y = p_y
	,	d_x = d_x
	,	d_y = d_y
	,	w   = w
	,	h   = h
	,	a   = a
	}, LCSS.Element_Reference.Metatable)
end

function LCSS.Element_Reference:Draw ()
	return self.Class:Draw (
		self.p_x, self.p_y,
		self.d_x, self.d_y,
		self.w, self.h, self.a)
end

function LCSS.Element_Reference:Detect (point_x, point_y, List)
	return self.Class:Detect (
		point_x, point_y,
		self.p_x, self.p_y,
		self.d_x, self.d_y,
		self.w, self.h, self.a,
		List)
end

function LCSS:Reference (p_x, p_y, d_x, d_y, w, h, a)
	return LCSS.Element_Reference.New (self, p_x, p_y, d_x, d_y, w, h, a)
end

LCSS.Classes = {}
LCSS.Class_Names = {}

LCSS.Defaults =
{	padding = 0
,	left    = false
,	right   = false
,	top     = false
,	bottom  = false

,	id      = false
,	hidden  = false

,	cache_x = false
,	cache_y = false
,	cache_w = false
,	cache_h = false
}

function LCSS:Build ()
	self.left   = self.left   or self.padding
	self.right  = self.right  or self.padding
	self.top    = self.top    or self.padding
	self.bottom = self.bottom or self.padding
	return self:Class_build()
end

function LCSS:Build_rgba ()
	self.red   = self.red   or self.rgba[1]
	self.green = self.green or self.rgba[2]
	self.blue  = self.blue  or self.rgba[3]
	self.alpha = self.alpha or self.rgba[4]
	return self
end

function LCSS:Use_rgba ()
	love.graphics.setColor (self.red, self.green, self.blue, self.alpha)
end

function LCSS:Draw (p_x, p_y, d_x, d_y, w, h, a)
	if self.hidden then return true end

	p_x = p_x + a*(self.left)   p_y = p_y + a*(self.top)
	d_x = d_x + a*(self.left)   d_y = d_y + a*(self.top)
	w = w - a*(self.left + self.right)
	h = h - a*(self.top + self.bottom)

	self.cache_x = p_x
	self.cache_y = p_y
	self.cache_w = w
	self.cache_h = h

	if w <= 0 or h <= 0 then return false end

	local canvas_old, canvas_new
	if self.use_canvas then
		canvas_old = love.graphics.getCanvas ()
		canvas_new = love.graphics.newCanvas (w,h)
		love.graphics.setCanvas (canvas_new)
	end

	local ret = self:Class_draw (d_x, d_y, w, h, a)

	local References_Child = self
	:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	if References_Child then
		for i = 1, #References_Child do
			local reference = References_Child [i]
			ret = reference:Draw () and ret
		end
	else
		ret = false
	end

	local References_Element = self
	:Class_element_arrange (p_x, p_y, d_x, d_y, w, h, a)
	if References_Element then
		for i = 1, #References_Element do
			local reference = References_Element [i]
			ret = reference:Draw () and ret
		end
	else
		ret = false
	end

	if self.use_canvas then
		love.graphics.setCanvas (canvas_old)
		love.graphics.setColor (1,1,1,1)
		love.graphics.draw (canvas_new, d_x, d_y)
		canvas_new:release ()
	end

	return ret
end

function LCSS:Detect (point_x, point_y, List)
	List = List or {}

	if self.hidden
	or self.cache_w <= 0
	or self.cache_h <= 0
	then return List end

	local r_x = point_x - self.cache_x
	local r_y = point_y - self.cache_y

	if  0 <= r_x and r_x <= self.cache_w
	and 0 <= r_y and r_y <= self.cache_h
	then
		if self.id then
			table.insert (List, self.id)
		end

		for i = 1, #self do
			self[i]:Detect (point_x, point_y, List)
		end

		local Elements = self:Class_get_elements ()
		for i = 1, #Elements do
			Elements[i]:Detect (point_x, point_y, List)
		end
	end

	return List
end

--	Class override functions

function LCSS:Class_build ()
	return self
end

function LCSS:Class_draw (d_x, d_y, w, h, a)
	return true
end

function LCSS:Class_child_arrange (p_x, p_y, d_x, d_y, w, h, a)
	local List = {}
	for i = 1, #self do
		List [i] = self[i]:Reference (p_x, p_y, d_x, d_y, w, h, a)
	end
	return List
end

function LCSS:Class_get_elements ()
	return {}
end

function LCSS:Class_element_arrange (p_x, p_y, d_x, d_y, w, h, a)
	return {}
end

function LCSS.Load_class (name)
	assert (type (name) == "string"
	, "LCSS.Load_class expects a string")
	assert (LCSS.Classes[name] == nil
	, "[LCSS] Requested class was already loaded: ".. name)
	local Class = assert (require ("LCSS/".. name)
	, "[LCSS] Requested class not found: ".. name)
	table.insert (LCSS.Class_Names, name)
	setmetatable (Class, LCSS.Metatable)
	LCSS.Classes [name] = Class
	print ("[LCSS] Loaded class: ".. name)
end

function LCSS.New (class_name, Structure)
	assert (type (class_name) == "string"
	, "LCSS.New expects a string for class_name argument")
	local Class = assert (LCSS.Classes [class_name]
	, "[LCSS] New recieved unknown class: ".. class_name)
	assert (type (Structure) == "table"
	, "LCSS.New expects a table for Structure argument")
	LCSS.Table_default (Structure, LCSS.Defaults)
	LCSS.Table_default (Structure, Class.Defaults)
	setmetatable (Structure, Class.Metatable)
	return Structure:Build ()
end

function LCSS:Get_ids (List)
	List = List or {}
	if self.id then
		List [self.id] = self
	end
	for i = 1, #self do
		self[i]:Get_ids (List)
	end
	local Elements = self:Class_get_elements ()
	for i = 1, #Elements do
		Elements[i]:Get_ids (List)
	end
	return List
end

function LCSS.Load_file (path, Resources)
	assert (type (path) == "string"
	, "LCSS.Load_file expects string for path argument")
	local file = assert (io.open (path, "rb")
	, "LCSS.Load_file file not found")
	local text = file:read ("*a")
	file:close ()

	text = text
	:gsub ("<!", "--[[")
	:gsub ("!>", "]]")
	:gsub ("<%s*(%w+)%s*/", "%1 = </")
	:gsub ("</(.-):", "LCSS.New (\"%1\", {")
	:gsub ("/>", " }), ")
	:gsub ("%$(.-)%$", "R [%1]")
	:gsub ("<(.-)>", "%1,")
	:reverse ()
	:gsub(",", " ", 1)
	:reverse ()

	text = "return function (LCSS, R)\nreturn\n".. text .."end"

	local res = assert (load (text)
	, "Invalid syntax:\n".. text)
	res = assert (res ()
	, "Failed to build master constructor:\n".. text)
	res = assert (res (LCSS, Resources)
	, "Failed to build structure:\n".. text)

	print ("[LCSS] File loaded: ".. path)
	print (text)

	return res
end

function LCSS.Load_core ()
	local Core_Classes =
	{	"Absolute"
	,	"HDivider"
	,	"VDivider"
	,	"Empty"
	,	"Fill"
	,	"Image"
	,	"Ratio"
	,	"HScroller"
	,	"VScroller"
	,	"Text"
	}
	for i = 1, #Core_Classes do
		LCSS.Load_class (Core_Classes [i])
	end
end

return LCSS
