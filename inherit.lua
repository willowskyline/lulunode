-- Allows

-- function {table:type, table:metatable} InheritFrom (table:Type base)
function InheritFrom (base)
	assert (type (base) == "table", "Invalid base type")
	assert (type (base.__index) == "table", "Invalid base table (bad __index)")
	local new, newmeta = {}, {}								-- Gen new tables
	for k, v in pairs (base) do newmeta[k] = v end			-- copy all metatable enties
	for k, v in pairs (base.__index) do new[k] = v end		-- copy all type enties
	newmeta.__index = new									-- set metatable's __index
	return new, newmeta										-- return both tables
end
