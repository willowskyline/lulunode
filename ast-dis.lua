
--	function AstRef:Pretty ()
--		return "~>"..self.resolved:Pretty ()
--	end


local function TABS (depth)
	return (" "):rep (depth)
end

-- TO IR

local AstIR = {}

function AstIR:LIST (depth, label)
	local t = {TABS (depth), "[", label, " #", self.___uid, "\n"}
	local ins = table.insert

	for i, v in ipairs (self) do
		local f = AstIR[v.arity]
		if f then
			ins (t, f (v, depth + 1))
		else
			ins (t, TABS (depth + 1).."!"..v.arity.."!\n")
		end
	end
	ins (t, TABS (depth))
	ins (t, "]\n")
	return table.concat (t)
end

function AstIR:TOKEN (depth)
	local l = self.line
	local c = self.column
	if l == -1 and c == -1 then
		return "(token "..self.text.."@phantom)"
	end
	return "(token `"..self.text.."`@"..self.line..":"..self.column..")"
end

function AstIR:PATH (depth)
	local t = {"(path "}
	for i, v in ipairs (self) do
		table.insert (t, AstIR.TOKEN (v))
	end
	table.insert (t, ")")
	return table.concat (t)
end

function AstIR:namespace (depth, label)
	return TABS (depth).."(namespace #"..self.___uid.." "..AstIR.PATH (self:GetPath (), depth + 1).."\n"..AstIR.LIST (self:GetBody (), depth + 1, "body")..TABS (depth)..")\n"
end
function AstIR:byval (depth)
	return TABS (depth).."(byval #"..self.___uid.." "..AstIR.PATH (self:GetPath (), depth + 1).." "..table.concat (self:GetAllModifiers (), " ").."\n"..AstIR.LIST (self:GetBody (), depth + 1, "body")..TABS (depth)..")\n"
end
function AstIR:byref (depth)
	return TABS (depth).."(byref #"..self.___uid.." "..AstIR.PATH (self:GetPath (), depth + 1).." "..table.concat (self:GetAllModifiers (), " ").."\n"..AstIR.LIST (self:GetBody (), depth + 1, "body")..TABS (depth)..")\n"
end
function AstIR:bytie (depth)
	return TABS (depth).."(bytie #"..self.___uid.." "..AstIR.PATH (self:GetPath (), depth + 1).." "..table.concat (self:GetAllModifiers (), " ").."\n"..AstIR.LIST (self:GetBody (), depth + 1, "body")..TABS (depth)..")\n"
end
function AstIR:enum (depth, label)
	local from = self:GetFrom ()
	if from then
		from = " from "..from:Pretty ()
	else
		from = ""
	end
	return TABS (depth).."(enum #"..self.___uid.." "..AstIR.PATH (self:GetPath (), 0).." "..table.concat (self:GetAllModifiers (), " ")..from.."\n"..AstIR.LIST (self:GetBody (), depth + 1, "body")..TABS (depth)..")\n"
end
function AstIR:vardelc (depth)
	local v = self:GetInitalValue ()
	if v then
		v = " "..v:ToIr (depth + 1)
	else
		v = ""
	end
	print (self, v)
	return TABS (depth).."(vardelc #"..self.___uid.." "..table.concat (self:GetAllModifiers (), " ").." "..AstIR.PATH (self:GetPath (), 0)..v..")\n"
end
function AstIR:const (depth)
	local v = self:GetInitalValue ()
	if v then
		v = " "..v:ToIr (depth + 1)
	else
		v = ""
	end
	return TABS (depth).."(const #"..self.___uid.." "..table.concat (self:GetAllModifiers (), " ").." "..AstIR.PATH (self:GetPath (), 0)..v..")\n"
end

function AstIR:binary (depth)
	return TABS (depth).."("..self:GetOperator ().."#"..self.___uid.."\n"..TABS (depth + 1)..self:GetLeft ().."\n"..TABS (depth + 1)..self:GetRight ():Pretty (depth)
end
function AstIR:unary (depth)
	return TABS (depth).."("..self:GetOperator ().."#"..self.___uid.."\n"..TABS (depth + 1)..self:GetRight ():Pretty (depth)
end
function AstIR:nonary (depth)
	local op = self:GetOperator ()
	if op == "var" then
		return "(var #"..self.___uid.." "..self:GetPath ():ToIR (depth)..")"
	elseif op == "lit" then
		return "(lit #"..self.___uid.." "..self:GetLiteral ():ToIR (depth)..")"
	else
		error ()
	end
end

function Ast:ToIr ()
	local f = AstIR[self.arity]
	if f then
		return f (self, 0)
	end
	return "!"..self.arity.."!"
end

function AstList:ToIR ()
	return AstIR.LIST (self, 0)
end

function AstPath:ToIR ()
	return AstIR.PATH (self, 0)
end

function Token:ToIR ()
	return AstIR.TOKEN (self, 0)
end




-- EXTRACT INFO

local AstExtractInfo = {}

function AstExtractInfo.LIST (lst, info)
	for i, v in ipairs (lst) do
		AstExtractInfo[v.arity] (v, info)
	end
end


function AstExtractInfo:namespace (info)
	table.insert (info.namespace, self)
	AstExtractInfo.LIST (self:GetBody (), info)
end
function AstExtractInfo:enum (info)
	table.insert (info.enum, self)
	AstExtractInfo.LIST (self:GetBody (), info)
end
function AstExtractInfo:byref (info)
	table.insert (info.byref, self)
	AstExtractInfo.LIST (self:GetBody (), info)
end
function AstExtractInfo:byval (info)
	table.insert (info.byval, self)
	AstExtractInfo.LIST (self:GetBody (), info)
end
function AstExtractInfo:bytie (info)
	table.insert (info.bytie, self)
	AstExtractInfo.LIST (self:GetBody (), info)
end


function Ast:ExtractInfo ()

end




-- PRETTY

local AstPretty = {}

function AstPretty:LIST (depth)
	local t = {}
	local ins = table.insert

	for i, v in ipairs (self) do
		ins (t, v:Pretty (depth + 1))
	end
	return table.concat (t)
--	return TABS (depth).."/[ body ]/\n"
end

function AstPretty:PATH (depth)
	local ins = table.insert
	local t = {}
	for i, v in ipairs (self) do
		if i ~= 1 then ins (t, "::") end
		ins (t, AstPretty.TOKEN (v))
	end
	return table.concat (t)
end

function AstPretty:TYPE (depth)
	print (self:ToString ())
	if not self:IsPolymorphic () then
		return AstPretty.PATH (self:GetPath ())
	end
	local t = {AstPretty.PATH (self:GetPath ()), "<"}
	local ins = table.insert
	for i, v in ipairs (self) do
		if i ~= 1 then ins (t, ",") end
		ins (t, AstPretty.TYPE (v))
	end
	ins (t, ">")
	return table.concat (t)
end

function AstPretty:TOKEN (depth)
	return self.text
end

function AstPretty:namespace (depth)
	return TABS (depth).."namespace "..AstPretty.PATH (self:GetPath (), depth).."\n"..AstPretty.LIST (self:GetBody (), depth + 1)..TABS (depth).."end\n"
end
function AstPretty:byref (depth)
	return TABS (depth).."byref "..AstPretty.PATH (self:GetPath (), depth).."\n"..AstPretty.LIST (self:GetBody (), depth + 1)..TABS (depth).."end\n"
end
function AstPretty:byval (depth)
	return TABS (depth).."byval "..AstPretty.PATH (self:GetPath (), depth).."\n"..AstPretty.LIST (self:GetBody (), depth + 1)..TABS (depth).."end\n"
end
function AstPretty:bytie (depth)
	return TABS (depth).."bytie "..AstPretty.PATH (self:GetPath (), depth).."\n"..AstPretty.LIST (self:GetBody (), depth + 1)..TABS (depth).."end\n"
end
function AstPretty:enum (depth)
	local ins = table.insert
	local t = {TABS (depth)}
	for i, v in ipairs (self:GetAllModifiers ()) do
		ins (t, tostring (v))
		ins (t, " ")
	end
	ins (t, "enum ")
	ins (t, AstPretty.PATH (self:GetPath (), depth))
	if self:GetFrom () then
		ins (t, " from ")
		int (t, self:GetFrom ():Pretty ())
	end
	ins (t, "\n")
	ins (t, AstPretty.LIST (self:GetBody (), depth + 1))
	ins (t, TABS (depth))
	ins (t, "end\n")
	return table.concat (t)
end
AstPretty["function"] = function (self, depth)
	local ins = table.insert
	local t = {TABS (depth)}
	for i, v in ipairs (self:GetAllModifiers ()) do
		ins (t, tostring (v))
		ins (t, " ")
	end
	ins (t, "function ")
	for i, v in ipairs (self:GetAllReturnTypes ()) do
		if i ~= 1 then ins (", ") end
		ins (t, AstPretty.TYPE (v, depth))
	end
	ins (t, " ")
	ins (t, AstPretty.PATH (self:GetPath (), depth))
	ins (t, " (")
	for i, v in ipairs (self:GetArguments ()) do
		if i ~= 1 then ins (t, ", ") end
		ins (t, AstPretty.argument (v, depth))
	end
	ins (t, ")\n")
	ins (t, AstPretty.LIST (self:GetBody (), depth + 1))
	ins (t, TABS (depth))
	ins (t, "end\n")
	return table.concat (t)
end
function AstPretty:argument (depth)
	if self:GetType () then
		return AstPretty.TYPE (self:GetType (), depth).." "..AstPretty.PATH (self:GetPath (), depth)
	end
	return AstPretty.PATH (self:GetPath (), depth)
end
function AstPretty:vardelc (depth)
	local ins = table.insert
	local t = {TABS (depth)}
	for i, v in ipairs (self:GetAllModifiers ()) do
		ins (t, tostring (v))
		ins (t, " ")
	end
	if self:GetType () then
		ins (t, AstPretty.TYPE (self:GetType (), depth + 1))
	else
		ins (t, "/[any]/")
	end	
	ins (t, " ")
	ins (t, AstPretty.PATH (self:GetPath (), depth))	
	local v = self:GetInitalValue ()
	if v then
		ins (t, " = ")		
		ins (t, tostring (v:Pretty (depth)))
	end
	ins (t, "\n")
	return table.concat (t)
end
function AstPretty:const (depth)
	local ins = table.insert
	local t = {TABS (depth)}
	for i, v in ipairs (self:GetAllModifiers ()) do
		ins (t, tostring (v))
		ins (t, " ")
	end
--	if self:GetType () then
--		ins (t, AstPretty.TYPE (self:GetType (), depth + 1))
--	else
--		ins (t, "/[any]/")
--	end	
--	ins (t, " ")
	ins (t, AstPretty.PATH (self:GetPath (), depth))	
	local v = self:GetInitalValue ()
	if v then
		ins (t, " = ")		
		ins (t, tostring (v:Pretty (depth)))
	end
	ins (t, "\n")
	return table.concat (t)
end
AstPretty["return"] = function (self, depth)
	local ins = table.insert
	local t = {TABS (depth), "return "}
	for i, v in ipairs (self:GetArguments ()) do
		if i ~= 1 then ins (t, ", ") end
		ins (t, v:Pretty (depth))
	end
	ins (t, "\n")
	return table.concat (t)
end

function AstPretty:binary (depth)
	local op = self:GetOperator ()
	if Lexer._GetLbpOf (nil, op) > 115 then
		return self:GetLeft ():Pretty (depth)..self:GetOperator ()..self:GetRight ():Pretty (depth)
	end
	return self:GetLeft ():Pretty (depth).." "..self:GetOperator ().." "..self:GetRight ():Pretty (depth)
end
function AstPretty:unary (depth)
	return self:GetOperator ().." "..self:GetRight ():Pretty (depth)
end
function AstPretty:nonary (depth)
	local op = self:GetOperator ()
	if op == "var" then
		return self:GetPath ():Pretty (depth)
	elseif op == "lit" then
		return self:GetLiteral (depth):Pretty (depth)
	else
		error ()
	end
end

function AstPretty:new (depth)
	return "new ? ? (???)"
end

function Ast:Pretty (optional_depth)
	local f = AstPretty[self.arity]
	if f == nil then
		return "/["..self.arity.."/]"
	end
	return f (self, optional_depth or 0)
end
function AstType:Pretty (optional_depth)
	return AstPretty.TYPE (self, optional_depth or 0)
end
function AstPath:Pretty (optional_depth)
	return AstPretty.PATH (self, optional_depth or 0)
end
function Token:Pretty (optional_depth)
	return AstPretty.TOKEN (self, optional_depth or 0)
end