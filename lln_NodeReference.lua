

lln_InputReference = {}
lln_InputReference.Metatable =
{__index = lln_InputReference}

function lln_InputReference.New (T)
	return setmetatable (
		{	node = false	--	output node
		,	pin  = false	--	output pin
		,	T    = T		--	current T
		,	dT   = T		--	default T
		}
	,	lln_InputReference.Metatable )
end

function lln_InputReference:ResolveType ()
	return self.node
	   and self.node.ioref[self.pin]
	    or self.dT
end
