
lln_User =
{	hand = false
,	system = false
,	hook = RSkyl.NilFunction
,	hooks = {}
,	cursors = {}
,	snap = false
,	islinking = false
,	selected = false
}

function lln_User.LoadCursors ()
	local cursors = {"default", "grab", "in", "out", "resize", "visual", "box", "none"}
	for i,v in ipairs(cursors) do
		lln_User.cursors[v] = love.mouse.newCursor ("cursors/"..v..".png", 10, 10)
	end
end

function lln_User.EmptyHand ()
	lln_User.hand = false
	lln_User.islinking = false
	lln_User.hook = RSkyl.NilFunction
	love.textinput = RSkyl.NilFunction
	love.keypressed = RSkyl.NilFunction
	love.mousepressed = lln_User.EmptyHandPress
end

function lln_User.SnapFunction (a)
	--       ___ grid size ___   rounder
	--      /                 \     |
	return 10 * math.floor (a/10 + 0.5)
end

function lln_User.hooks.GrabInHand ()
	local x,y = love.mouse.getPosition ()
	x = x - lln_User.dx
	y = y - lln_User.dy
	if lln_User.snap then
		x = lln_User.SnapFunction (x)
		y = lln_User.SnapFunction (y)
	end
	lln_User.hand.node.x = x
	lln_User.hand.node.y = y
end

function lln_User.hooks.ResizeInHand ()
	local x,y = love.mouse.getPosition ()
	local w = lln_User.ow + x - lln_User.ox
	if lln_User.snap then
		w = lln_User.SnapFunction (w)
	end
	lln_User.hand.node.width = math.max ( w, lln_User.hand.node.definition.width )
	table.insert (lln_User.system.updatequeue, lln_User.hand.node)
end

function lln_User.hooks.MultiSelect ()
	lln_User.x1, lln_User.y1 = love.mouse.getPosition ()
end

function lln_User.hooks.MultiMove ()
	local x,y = love.mouse.getPosition ()
	x = x - lln_User.x1
	y = y - lln_User.y1
	for i,r in ipairs(lln_User.hand) do
		local n = r.node
		n.x = r.ox + x
		n.y = r.oy + y
		if lln_User.snap then
			n.x = lln_User.SnapFunction (n.x)
			n.y = lln_User.SnapFunction (n.y)
		end
	end
end

function lln_User.EmptyHandPress (x, y, button)
	
	if button ~= 1 and button ~= 2 then
		lln_User.snap = not lln_User.snap
		lln_User.system:BuildAST()
		return nil
	end
	
	local detect = lln_User.system:Detect (x, y)
	if not detect then
		
		if button == 2 then
			lln_User.selected = false
		end
		
		lln_User.hand = "multi"
		lln_User.x0 = x
		lln_User.y0 = y
		
		lln_User.hook = lln_User.hooks.MultiSelect
		lln_User.hook ()
		love.mousepressed = lln_User.MultiSelectPress
		
		return
	end
	
	lln_User.hand = detect
	
	if detect.sub == "grab" then
		
		lln_User.ox = detect.node.x
		lln_User.oy = detect.node.y
		lln_User.dx = x - lln_User.ox
		lln_User.dy = y - lln_User.oy
		
		lln_User.hook = lln_User.hooks.GrabInHand
		love.mousepressed = lln_User.GrabInHandPress
		
	elseif detect.sub == "resize" then
		
		lln_User.ox = x
		lln_User.ow = detect.node.width
		
		lln_User.hook = lln_User.hooks.ResizeInHand
		love.mousepressed = lln_User.ResizeInHandPress
		
	elseif detect.sub == "box" then
		
		lln_User.ot = detect.node.ioref [detect.comp]
		love.textinput = lln_User.BoxInHandTextInput
		love.keypressed = lln_User.BoxInHandKeyPressed
		love.mousepressed = lln_User.BoxInHandPress
		
	elseif detect.sub == "in" then
		
		if button == 1 then
			lln_User.px, lln_User.py = detect.node:GetPinPosition (detect.comp)
			lln_User.islinking = "out"
			love.mousepressed = lln_User.PinInHandPress
		else
			local ref = detect.node.ioref [detect.comp]
			ref.node = false
			ref.pin  = false
			--detect.node:DoHook ()
			--table.insert (lln_User.system.updatequeue, detect.node)
			return lln_User.EmptyHand()
		end
		
	elseif detect.sub == "out" then
		
		lln_User.px, lln_User.py = detect.node:GetPinPosition (detect.comp)
		lln_User.islinking = "in"
		love.mousepressed = lln_User.PinInHandPress
		
	end
	
end
love.mousepressed = lln_User.EmptyHandPress

function lln_User.GrabInHandPress (x, y, button)
	if button == 1 then
		lln_User.hook()
		
		lln_User.system:SortSyncList()
		if lln_User.hand.node.definition.sync
		and lln_User.system:CheckSyncViolations()
		then
			return lln_User.GrabInHandPress (x, y, 2)
		end
		return lln_User.EmptyHand()
	elseif button == 2 then
		lln_User.hand.node.x = lln_User.ox
		lln_User.hand.node.y = lln_User.oy
		lln_User.system:SortSyncList()
		return lln_User.EmptyHand()
	else
		lln_User.snap = not lln_User.snap
	end
end

function lln_User.ResizeInHandPress (x, y, button)
	if button == 1 then
		lln_User.hook()
		return lln_User.EmptyHand()
	elseif button == 2 then
		table.insert (lln_User.system.updatequeue, lln_User.hand.node)
		lln_User.hand.node.width = lln_User.ow
		return lln_User.EmptyHand()
	else
		lln_User.snap = not lln_User.snap
	end
end

function lln_User.BoxInHandTextInput (t)
	local c = lln_User.hand.comp
	local io = lln_User.hand.node.ioref
	io[c] = io[c] .. t
	table.insert (lln_User.system.updatequeue, lln_User.hand.node)
end

function lln_User.BoxInHandKeyPressed (key)
	local c = lln_User.hand.comp
	local io = lln_User.hand.node.ioref
	local t = io[c]
	
	if key == "backspace" then
--[[		local off = utf8.offset(t, -1)
		if off then
			io[c] = t:sub (1, off-1)
		end--]]
		io[c] = t:sub (1,-2)
		table.insert (lln_User.system.updatequeue, lln_User.hand.node)
	elseif key == "return" or key == "kpenter" then
		lln_User.BoxInHandPress (0,0,1)
	elseif key == "escape" then
		lln_User.BoxInHandPress (0,0,2)
	end
end

function lln_User.BoxInHandPress (x, y, button)
	table.insert (lln_User.system.updatequeue, lln_User.hand.node)
	if button == 1 then
		return lln_User.EmptyHand()
	elseif button == 2 then
		local c = lln_User.hand.comp
		local io = lln_User.hand.node.ioref
		io[c] = lln_User.ot
		return lln_User.EmptyHand()
	else
		lln_User.snap = not lln_User.snap
	end
end

function lln_User.PinInHandPress (x, y, button)
	if button == 1 then
		local detect = lln_User.system:Detect (x, y)
		if not detect then return nil end
		if detect.sub == lln_User.islinking then
			
			local  srcref = detect.sub == "out" and detect or lln_User.hand
			local destref = detect.sub == "in"  and detect or lln_User.hand
			
			local oldref = destref.node.ioref [destref.comp]
			local newref = lln_InputReference.New (oldref.dT)
			newref.node = srcref.node
			newref.pin = srcref.comp
			
			destref.node.ioref [destref.comp] = newref
			
			
			if lln_CycleFinder.InsertionCheck (destref.node)	--	value only search
			or lln_User.system:CheckSyncViolations ()
			--or not destref.node:DoHook ()						--	hook match
			then
				destref.node.ioref [destref.comp] = oldref
			else
				table.insert (lln_User.system.updatequeue, destref.node)
				return lln_User.EmptyHand ()
			end			
		end
	elseif button == 2 then
		return lln_User.EmptyHand()
	else
		lln_User.snap = not lln_User.snap
	end
end

function lln_User.MultiSelectPress (x, y, button)
	
	lln_User.hook()
	
	local lx = math.min(lln_User.x0, lln_User.x1)
	local rx = math.max(lln_User.x0, lln_User.x1)
	local ty = math.min(lln_User.y0, lln_User.y1)
	local by = math.max(lln_User.y0, lln_User.y1)
	
	lln_User.hand = {}
	
	for i,n in ipairs(lln_User.system) do
		local nlx = n.x + 10
		local nrx = nlx + n.width
		local nty = n.y
		local nby = nty + n.height
		
		if  lx <= nlx and nrx <= rx
		and ty <= nty and nby <= by
		then
			local ox = n.x
			local oy = n.y
			table.insert (lln_User.hand, {node=n,ox=ox,oy=oy})
		end
	end
	
	if #lln_User.hand == 0 then
		return lln_User.EmptyHand ()
	end
	
	lln_User.hook = lln_User.hooks.MultiMove
	love.mousepressed = lln_User.MultiMovePress
end

function lln_User.MultiMovePress (x, y, button)
	if button ~= 1 and button ~= 2 then
		lln_User.snap = not lln_User.snap
		return nil
	end
	lln_User.hook ()
	lln_User.system:SortSyncList ()
	if lln_User.system:CheckSyncViolations() then
		button = 2
	end
	--[[
	local tmp = lln_User.hand
	for i,r in ipairs(tmp) do
		lln_User.hand = r
		lln_User.dx = r.dx
		lln_User.dy = r.dy
		lln_User.ox = r.ox
		lln_User.oy = r.oy
		lln_User.GrabInHandPress (x, y, button)
	end
	--]]
	if button == 2 then
		for i,r in ipairs(lln_User.hand) do
			r.node.x = r.ox
			r.node.y = r.oy
		end
	end
	return lln_User.EmptyHand ()
end

