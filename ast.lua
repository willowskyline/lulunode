require "inherit"

----------------------------------------------------------------
--   A B S T R A C T   B A S E
----------------------------------------------------------------
-- Declares common functions to all AST related things
-- Each throws an error and is expected to be overriden (excl. __tostring)
AbstractAstBase = {}
AbstractAstBaseMetatable = {__index = AbstractAstBase}

--! Forbids creating members in the table requiring the use of provided safe methods
function AbstractAstBaseMetatable:__newindex (index, value)
	error ("Cannot set protected key "..luax.ToStringWithType (index).. " to value "..luax.ToStringWithType (value))
end
--! Alias of `self:ToString ()`
function AbstractAstBaseMetatable:__tostring ()
	return (self:ToString ())
--	error ("Expected to be implemented in derived types "..(luax.GetMetatableName(self)or"?").."__tostring()")
end
--! Alias of `self:ToShortStringString ()`
function AbstractAstBaseMetatable:__toshortstring ()
	return (self:ToShortString ())
--	error ("Expected to be implemented in derived types "..(luax.GetMetatableName(self)or"?").."__tostring()")
end
--! Returns an the internal form for debugging
function AbstractAstBase:ToString ()
	error ("Expected to be implemented in derived types "..(luax.GetMetatableName(self)or"?")..":ToString()")
end
--! Returns an the internal form for debugging, safe avoiding cyclic refences
function AbstractAstBase:ToShortString ()
	error ("Expected to be implemented in derived types "..(luax.GetMetatableName(self)or"?")..":ToShortString()")
end
--! Returns a pretty from of the object
function AbstractAstBase:Pretty ()
	error ("Expected to be implemented in derived types "..(luax.GetMetatableName(self)or"?")..":Pretty()")
end


----------------------------------------------------------------
--   A S T   P A T H
----------------------------------------------------------------
AstPath, AstPathMetatable = InheritFrom (AbstractAstBaseMetatable)

function AstPath.new (tableOfTokens)
	assert (#tableOfTokens > 0, "tableOfTokens should not be empty")
	return setmetatable (luax.clonetab (tableOfTokens), AstPathMetatable)
end

function AstPathMetatable:__newindex (index, value)
	if AstPathMetatable.___AllowWrites then
		rawset (self, index, value)
	else
		error ("Cannot set protected key "..luax.ToStringWithType (index).. " to value "..luax.ToStringWithType (value))
	end
end

function AstPath:ToString ()
	assert (#self > 0, "tableOfTokens should not be empty")
	
	local r = {}
	local ins = table.insert
	for i, v in ipairs (self) do
		ins (r, tostring (v))
	end
	return "AstPath{"..table.concat (r, "::").."}"
end

AstPath.ToShortString = AstPath.ToString


function AstPath:IsRooted ()
	assert (#self > 0, "tableOfTokens should not be empty")

	return self[1].text == "global"
end

function AstPath:AddPrefixPath (path)
	assert (#self > 0, "tableOfTokens should not be empty")
	
	assert (not self:IsRooted (), "Attempted to add prefix path to rooted path")

	if getmetatable (path) == AstPathMetatable then
		AstPathMetatable.___AllowWrites = true
		for i, v in ipairs (path) do
			table.insert (self, i, v)
		end
		AstPathMetatable.___AllowWrites = false
	elseif getmetatable (path) == TokenMetatable then
		AstPathMetatable.___AllowWrites = true
		table.insert (self, 1, path)
		AstPathMetatable.___AllowWrites = false
	else
		error ("Attempted to add non prefix path object of type "..luax.ToStringWithType (path))
	end
end

function AstPath:GetName ()
	assert (#self > 0, "tableOfTokens should not be empty")
	return self[#self]
end


----------------------------------------------------------------
--   A S T   T Y P E
----------------------------------------------------------------
AstType, AstTypeMetatable = InheritFrom (AbstractAstBaseMetatable)

function AstType.new (base, poly)
	assert (getmetatable (base) == AstPathMetatable, "base should be a AstPath is "..luax.ToStringWithType (base))
	local self = setmetatable ({base = base}, AstTypeMetatable)
	if type (poly) ~= "nil" then
		assert (type (poly) == "table", "poly should be a list")
		assert (#poly > 0, "if poly (polymorphic) is given it shouldn't be empty")
		for i, v in ipairs (poly) do
			assert (getmetatable (v) == AstTypeMetatable, "poly["..i.."] should be AstType")
			self[i] = v
		end
	end
	return self
end

function AstType:ToString ()
	local r = {"AstType{", self:GetPath ():ToString (), "<" }
	local ins = table.insert
	for i, v in ipairs (self) do
		if i ~= 1 then ins (r, ",") end
		r[#r+1] = v:ToString ()
	end
	ins (r, ">}")
	return table.concat (r)
end
AstType.ToShortString = AstType.ToString

function AstType:IsPolymorphic ()
	return #self > 0
end

function AstType:GetPath ()
	return self.base
end

function AstType:IsRooted ()
	return self:GetPath ():IsRooted ()
end

function AstType:TransmuteToName ()
	assert (not self:IsPolymorphic (), "Complex AstType could not be transmute to name")
	return self:GetPath ()
end


----------------------------------------------------------------
--   A S T   R E F E R N C E
----------------------------------------------------------------
AstRef, AstRefMetatable = InheritFrom (AbstractAstBaseMetatable)

function AstRef:ToShortString ()
	return "AstRef#"..self.___uid.."{^#"..(self.parent and self.parent.___uid or "?").."~>"..self.resolved:ToShortString ()
end
function AstRef:ToString ()
	return "AstRef#"..self.___uid.."{^#"..(self.parent and self.parent.___uid or "?").."~>"..self.resolved:ToShortString ()
end
function AstRef.___new (resolved)
	return setmetatable ({___uid = Ast.___newuid (), resolved = resolved}, AstRefMetatable)
end

----------------------------------------------------------------
--   A S T
----------------------------------------------------------------
Ast, AstMetatable = InheritFrom (AbstractAstBaseMetatable)

function Ast.___newuid ()
	local v = Ast.___current_uid
	if v == nil then v = 0 else v = v + 1 end
	Ast.___current_uid = v
	return v
end

Ast.___valid_arities = {
	["namespace"]		= true,		["byval"]		= true,		["byref"]		= true,		["bytie"]		= true,
	["for"]				= true,		["while"]		= true,		["if"]			= true,		["repeat"]		= true,
	["binary"]			= true,		["unary"]		= true,		["nonary"]		= true,		["enum"]		= true,
	["vardelc"]			= true,		["function"]	= true,		["signature"]	= true,		["argument"]	= true,
	["new"]				= true,		["return"]		= true,		["call"]		= true,		["const"]		= true,
}
Ast.___valid_operators = {
	nonary =
	{
		["lit"]			= true,		["var"]			= true,
	},
	unary =
	{
		["+"]			= true,		["-"]			= true,		["~"]			= true,		["!"]			= true,
		["#"]			= true,		["++$"]			= true,		["$++"]			= true,		["--$"]			= true,
		["$--"]			= true,		["not"]			= true,
	},
	binary =
	{
		["+"]			= true,		["+="]			= true,		["-"]			= true,		["-="]			= true,
		["/"]			= true,		["/="]			= true,		["*"]			= true,		["*="]			= true,
		["%"]			= true,		["%="]			= true,		["^"]			= true,		["^="]			= true,
		["~="]			= true,		["!="]			= true,		["<"]			= true,		["<="]			= true,
		[">"]			= true,		[">="]			= true,		["<<"]			= true,		["<<="]			= true,
		[">>"]			= true,		[">>="]			= true,		["><<"]			= true,		["><<="]		= true,
		[">><"]			= true,		[">><="]		= true,		["="]			= true,		["=="]			= true,
		[".."]			= true,		["..="]			= true,		["<-"]			= true,		["->"]			= true,
		["."]			= true,		[":"]			= true,		["::"]			= true,		["["]			= true,
		["and"]			= true,		["or"]			= true,		["xor"]			= true,		["is"]			= true,
		["as"]			= true,		["&"]			= true,		["|"]			= true,
	},
	ternary =
	{
		["?"] = true,
--		["-["] = true,		-- a possible operator "source -[N]> output" basically moves N things from source to dest
	}
}


--! function Token Ast.new (Token token, string arity)
--! Token is the instigator or instantiator of the AST, it is used for error tracking/debugging
function Ast.new (token, arity)
	assert (type (token) ~= "nil", "Ast.new() argument 'token' cannot be nil")
	assert (type (arity) == "string", "Ast.new() argument 'arity' needs to be string")
	assert (Ast.___valid_arities[arity], "Ast.new() argument 'arity' ("..arity..") is not valid name")

	return setmetatable ({
		___uid = Ast.___newuid (),
		token = token,
		arity = arity,
	}, AstMetatable)
end

function Ast:ToString ()
	local insert = table.insert
	local sb = {"Ast#"..self.___uid.."\n  {^#"..(self.parent and self.parent.___uid or "?")}
	for k, v in pairs (self) do
		if k ~= "parent" and k ~= "___uid" then
			insert (sb, tostring (k).."="..tostring (v))
		end
	end
	return table.concat (sb, "\n    ").."\n  }"
end

function Ast:ToShortString ()
	return "Ast#"..self.___uid.."{^#"..(self.parent and self.parent.___uid or "?").." arity:"..(self.arity or "?").." operator:"..(self.operator or "?").."}"
end


function Ast:RefOf ()
--	assert (self:IsRooted (), "(PRECAUTIONARY) allow RefOf a non Rooted type?")
	assert (self.arity == "enum"		or self.arity == "argument"
			or self.arity == "byref"	or self.arity == "byval"
			or self.arity == "bytie"	or self.arity == "enum"
			or self.arity == "function" or self.arity == "vardelc",
	"Cannot create a refence to "..self.arity)
	return AstRef.___new (self)
end

function Ast:AddArgument (child)
	assert (child.parent == nil, "Ast:AddArgument() cannot add, child already has a parent")

	if self.arity == "function" then
		assert (getmetatable (child) == AstMetatable, "Not a valid child type is "..luax.GetMetatableName (child).." expected AstMetatable")

		-- add as an argument to func
		assert (child.arity == "argument", "Not a valid child arity")
		local l = self.second
		rawset (l, #l + 1, child)
		rawset (child, "parent", l)

	elseif self.arity == "return" then
		assert (getmetatable (child) == AstMetatable or getmetatable (child) == AstPathMetatable, "Not a valid child type is "..luax.GetMetatableName (child).." expected AstMetatable or AstPathMetatable")

		-- add as an argument to return
		local l = self.first
		rawset (l, #l + 1, child)
		rawset (child, "parent", l)

	elseif self.arity == "call" or self.arity == "new" then
		assert (getmetatable (child) == AstMetatable or getmetatable (child) == AstPathMetatable, "Not a valid child type is "..luax.GetMetatableName (child).." expected AstMetatable or AstPathMetatable")

		-- add as an argument to call / new
		local l = self.second
		rawset (l, #l + 1, child)
		rawset (child, "parent", l)

	else
		error ("Cannot AddArgument to "..self.arity)
	end
end

--! Returns the arguments for the ast node
--! \note the return statement is considered as having arguments
function Ast:GetArguments ()

	if self.arity == "function" then
		return self.second

	elseif self.arity == "return" then
		return self.first

	elseif self.arity == "call" or self.arity == "new" then
		return self.second

	else
		error ("Cannot GetArguments from "..self.arity)
	end
end

function Ast:AddReturnType (child)
	assert (self.arity == "function")
	assert (child.parent == nil, "Ast:AddArgument() cannot add, child already has a parent")
	assert (getmetatable (child) == AstTypeMetatable or getmetatable (child) == AstRefMetatable, "Not a valid child type "..luax.ToStringWithType (child))
	self.first:AppendRaw (child)
end

function Ast:GetAllReturnTypes (child)
	assert (self.arity == "function")
	return self.first
end

function Ast:GetBody ()
	assert (self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "enum", "Cannot get body of '"..self.arity.."'")
	return self.body
end

function Ast:SetBody (body)
	assert (self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "enum", "Cannot get body of '"..self.arity.."'")
	assert (getmetatable (body) == AstListMetatable or getmetatable (body) == SsaListMetatable, "Set body msut be a AST or SSA list")
	rawset (self, "body", body)
end

function Ast:AddToBody (child)
	assert (child ~= nil, "child cannot be nil")
	assert (getmetatable (child) == AstMetatable, "Not a valid child to add to body "..luax.ToStringWithType (child))
	assert (child.parent == nil, "Child already has parent")
	assert (self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "enum", "Cannot add to to body of '"..self.arity.."'")
	local b = self.body
	rawset (b, #b + 1, child)
	rawset (child, "parent", b)
end

function Ast:AddManyToBody (children)
	for _, v in ipairs (children) do
		self:AddToBody (v)
	end
end

function Ast:AddModifer (mod)
	local m = self.mods
	if not m then
		m = {}
		rawset (self, "mods", m)
	end

	if self.arity == "function" then
		assert (mod == "explicit" or mod == "implicit" or mod == "unchecked" or mod == "volatile")
		assert (m[mod] == nil, "Modifer '"..mod.."' was already set")
		m[mod] = true
--		io.write ("added ", mod, " to ", self:ToShortString (), "\n")
	elseif self.arity == "vardelc" then
		assert (mod == "global" or mod == "local" or mod == "volatile" or mod == "const")
		assert (m[mod] == nil, "Modifer '"..mod.."' was already set")
		m[mod] = true
--		io.write ("added ", mod, " to ", self:ToShortString (), "\n")
	else
		error ("Ast:AddModifer() invalid AST type '"..self.arity.."' for modifer '"..mod.."'")
	end
end

function Ast:GetAllModifiers ()
	local m = self.mods
	if not m then return {} end

	if self.arity == "function" or self.arity == "vardelc" then
		local ml = {}
		for k in pairs (m) do table.insert (ml, k) end
		return ml
	else
		io.stderr:write ("Ast:GetAllModifiers() invalid AST type for modifer\n")
	end
end

function Ast:HasModifer (mod)
	local m = self.mods
	if not m then
		return false
	end

	if self.arity == "function" then
		if mod ~= "explicit" and mod ~= "implicit" and mod ~= "unchecked" then
			io.stderr:write ("Ast:HasModifer(",mod,") isn't valid for ", self.arity, " (will always return false)\n")
		end
		return m[mod]

	elseif self.arity == "vardelc" then
		if mod ~= "volatile" and mod ~= "const" and mod ~= "local" and mod ~= "global" then
			io.stderr:write ("Ast:HasModifer(",mod,") isn't valid for ", self.arity, " (will always return false)\n")
		end
		return m[mod]

	else
		io.stderr:write ("Ast:HasModifer() invalid AST type for modifer\n")
	end
end

function Ast:SetFirst (child)
	assert (child ~= nil, "Child cannot be nil")
	assert (child.parent == nil, "Ast:SetFirst() cannot add, child already has a parent")

	if self.arity == "nonary" and self.operator == "var" then
		-- expect token for vars
		assert (getmetatable (child) == TokenMetatable)
	elseif self.arity == "nonary" and self.operator == "lit" then
		-- anything for literals
	elseif self.arity == "call" then
		-- must be an expression or a path (or a literal?)
		assert (getmetatable (child) == AstPathMetatable or getmetatable (child) == AstMetatable, "Not a valid child; AstPath, Ast only")
	else
		-- AST only
		assert (getmetatable (child) == AstMetatable, "Not a valid child ("..(luax.GetMetatableName (child) or "?")..") for arity "..self.arity)
	end

	rawset (self, "first", child)
	rawset (child, "parent", self)
end
function Ast:GetFirst ()
	return self.first
end

function Ast:SetSecond (child)
	assert (child ~= nil, "Child cannot be nil")
	assert (child.parent == nil, "Ast:SetSecond() cannot add, child already has a parent")
	
	if self.arity == "nonary" then
		assert (self.operator == "lit", "only lit and const? have a operand to set")
		
	elseif self.arity == "const" or self.arity == "vardelc" then
			
	elseif self.arity == "unary" then
		error ("unary has no second operand to set")
		
	elseif self.arity == "binary" then
		assert (getmetatable (child) == AstMetatable or getmetatable (child) == AstPathMetatable,
			"Not a valid child ("..(luax.GetMetatableName (child) or "?")..") for arity "..self.arity)
	
	else
		error ("Unknown arity case "..self.arity)
	end	
	rawset (self, "second", child)
	rawset (child, "parent", self)
end
function Ast:GetSecond ()
	return self.second
end

function Ast:SetThird (child)
	assert (child ~= nil, "Child cannot be nil")
	assert (child.parent == nil, "Ast:SetThird() cannot add, child already has a parent")
	assert (getmetatable (child) == AstMetatable, "Not a valid child ("..(luax.GetMetatableName (child) or "?")..") for arity "..self.arity)
	rawset (self, "third", child)
	rawset (child, "parent", self)
end
function Ast:GetThird ()
	return self.third
end

function Ast:___GenBody ()
	self:___GenListMember ("body")
end

function Ast:___GenListMember (member)
	local l = AstList.new ()
	rawset (self, member, l)
	rawset (l, "parent", self)
end

----------------------------------------------------------------
-- Wrappers around SetFirst, SetSecond, SetThird for nicer reading of code
----------------------------------------------------------------

function Ast:GetLeft ()
	assert (self.arity == "binary")
	return self:GetFirst ()
end
function Ast:GetRight ()
	assert (self.arity == "binary" or self.arity == "unary")
	return self:GetSecond ()
end

function Ast:SetOperator (op)
	assert (self.arity == "nonary" or self.arity == "unary" or self.arity == "binary", "Ast:SetOp() cannot set, invalid arity for operator property")
	assert (Ast.___valid_operators[self.arity][op], "cannot set op '"..op.."' as it is not valid")
	rawset (self, "operator", op)
end

function Ast:GetOperator ()
	assert (self.arity == "nonary" or self.arity == "unary" or self.arity == "binary", "Ast:GetOp() cannot got, invalid arity for operator property")
	return self.operator
end

function Ast:SetPath (name)
	assert ((self.arity == "nonary" and self.operator == "var") or self.arity == "const" or self.arity == "argument" or self.arity == "new" or self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "vardelc" or self.arity == "enum", "Path not valid on AST '"..self.arity.."'")
	assert (getmetatable (name) == AstPathMetatable or getmetatable (name) == AstRefMetatable, "Path must be a AstPath or AstRef")
	rawset (self, "path", name)
end

function Ast:GetPath ()
	assert ((self.arity == "nonary" and self.operator == "var") or self.arity == "const" or self.arity == "argument" or self.arity == "new" or self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "vardelc" or self.arity == "enum", "Path not valid on AST '"..self.arity.."'")
	return self.path
end

--! gets the base type of an AST element
--! "from" semi-keyword
--! vaild for "byref", "bytie", "byval", "enum"
function Ast:GetFrom ()
	assert (self.arity == "byref" or self.arity == "bytie" or self.arity == "byval" or self.arity == "enum", "Cannot GetFrom of "..self.arity)
	return self.from
end

--! gets the base type of an AST element
--! "from" semi-keyword
--! vaild for "byref", "bytie", "byval", "enum"
function Ast:SetFrom (base)
	assert (self.arity == "byref" or self.arity == "bytie" or self.arity == "byval" or self.arity == "enum", "Cannot GetFrom of "..self.arity)
	assert (getmetatable (base) == AstMetatable)
	rawset (self, "from", base)
end

function Ast:GetName ()
	assert ((self.arity == "nonary" and self.operator == "var") or self.arity == "const" or self.arity == "argument" or self.arity == "new" or self.arity == "namespace" or self.arity == "function" or self.arity == "byval" or self.arity == "byref" or self.arity == "bytie" or self.arity == "vardelc" or self.arity == "enum", "Path not valid on AST '"..self.arity.."'")
	local path = self:GetPath ()
	if getmetatable (path) == AstRefMetatable then
		return path.resolved:GetName ()
	end
	assert (getmetatable (path) == AstPathMetatable, "Sanity check")
	return self:GetPath ():GetName ()
end

function Ast:GetType ()
	if self.arity == "vardelc" or self.arity == "new" or self.arity == "argument" then
		typ = self.first
	else
		error ("Cannot get type for arity "..self.arity)
	end

	assert (typ ~= nil, "Ast doesn't have an assigned type")
	assert (getmetatable (typ) == AstTypeMetatable, "CORRUPTION: Type was not a valid AstType")
	return typ
end

function Ast:SetInitalValue (expr)
	assert (self.arity == "vardelc" or self.arity == "const")
	assert (getmetatable (expr) == AstMetatable, "expr should be a AST")
	self:SetSecond (expr)
end

function Ast:GetInitalValue (expr)
	assert (self.arity == "vardelc" or self.arity == "const")
	return self:GetSecond (expr)
end

function Ast:SetType (typ)
	assert (getmetatable (typ) == AstTypeMetatable, "Type is not a valid AstType")
	if self.arity == "vardelc" or self.arity == "new" or self.arity == "argument" then
		assert (self.first == nil, "Ast already has a type")
		rawset (self, "first", typ)
	else
		error ("Cannot set type for arity "..self.arity)
	end
end

function Ast:GetType ()
	if self.arity == "vardelc" or self.arity == "new" or self.arity == "argument" then
		return self.first
	else
		error ("Cannot get type for arity "..self.arity)
	end
end

function Ast:SetLiteral (lit, typ)
	self:SetSecond (lit)
	if typ then self:SetType (typ) end
end
function Ast:GetLiteral ()
	return self:GetSecond ()
end

----------------------------------------------------------------
-- Nice constructors for common ASTs
----------------------------------------------------------------

function Ast.newBinary (token, op, left, right)
	local b = Ast.new (token, "binary")
	b:SetOperator (op)
	b:SetFirst (left)
	b:SetSecond (right)
	return b
end

function Ast.newUnary (token, op, left)
	local u = Ast.new (token, "unary")
	u:SetOperator (op)
	u:SetFirst (left)
	return u
end

--! function {Ast} Ast.newNamespace (Token token, AstPath name or nil)
function Ast.newNamespace (token, name)
	local n = Ast.new (token, "namespace")
	if name then n:SetPath (name) end
	n:___GenBody ()
	return n
end

--! function {Ast} Ast.newFunction (Token token, AstPath name or nil)
function Ast.newFunction (token, name)
	local f = Ast.new (token, "function")
	if name then f:SetPath (name) end
	f:___GenBody ()
	f:___GenListMember ("first")
	f:___GenListMember ("second")
	return f
end

--! function {Ast} Ast.newByval (Token token, AstPath name or nil)
function Ast.newByval (token, name)
	local n = Ast.new (token, "byval")
	if name then n:SetPath (name) end
	n:___GenBody ()
	return n
end
--! function {Ast} Ast.newBytie (Token token, AstPath name or nil)
function Ast.newBytie (token, name)
	local n = Ast.new (token, "bytie")
	if name then n:SetPath (name) end
	n:___GenBody ()
	return n
end
--! function {Ast} Ast.newByref (Token token, AstPath name or nil)
function Ast.newByref (token, name)
	local n = Ast.new (token, "byref")
	if name then n:SetPath (name) end
	n:___GenBody ()
	return n
end

function Ast.newVardelc (token, name, ofType)
	local v = Ast.new (token, "vardelc")
	v:SetPath (name)
	v:SetType (ofType)
	return v
end

function Ast.newArgument (token, name, ofType, alternative, restrict)
	local a = Ast.new (token, "argument")
	a:SetPath (name)
	if ofType then a:SetType (ofType) end
	if alternative then a:SetSecond (ofType) end
	if restrict then a:SetThird (restrict) end
	return a
end

function Ast.newVar (token, name)
	local v = Ast.new (token, "nonary")
	v:SetOperator ("var")
	v:SetPath (name)
	return v
end

function Ast.newLit (token, val, typ)
	local v = Ast.new (token, "nonary")
	v:SetOperator ("lit")
	v:SetLiteral (val, typ)
	return v
end

function Ast.newReturn (token, vals)
	local v = Ast.new (token, "return")
	if vals then
		assert (getmetatable (vals) == AstListMetatable, "vals is not a AstList")
		v:SetFirst (vals)
	else
		v:___GenListMember ("first")
	end
	return v
end

function Ast.newNew (token, typ, args)
	local v = Ast.new (token, "new")
	if typ then
		v:SetPath (typ)
	end
	if args then
		assert (getmetatable (args) == AstListMetatable, "argument 'args' is not a AstList")
		v:SetSecond (args)
	else
		v:___GenListMember ("second")
	end
	return v
end

function Ast.newCall (token, name, args)
	local v = Ast.new (token, "call")
	if name then
		v:SetFirst (name)
	end
	if args then
		assert (getmetatable (args) == AstListMetatable, "argument 'args' is not a AstList")
		v:SetSecond (args)
	else
		v:___GenListMember ("second")
	end
	return v
end

function Ast.newEnum (token, name, defs)
	local v = Ast.new (token, "enum")
	if name then
		v:SetPath (name)
	end
	if defs then
		assert (getmetatable (args) == AstListMetatable, "argument 'defs' is not a AstList")
		v:SetBody (defs)
	else
		v:___GenListMember ("body")
	end
	return v
end

function Ast.newConst (token, name, init)
	local v = Ast.new (token, "const")
	if name then
		v:SetPath (name)
	end
	if init then
		assert (getmetatable (args) == AstMetatable, "argument 'init' is not a Ast")
		v:SetFirst (defs)
	end
	return v
end

-- High Level Functions

--! returns the target for a call AST
function Ast:GetCallTarget ()
	return self.first
end

--! returns true if the AST is a operator func
function Ast:IsAnOperator ()
	assert (self.arity == "function", ":IsAnOperator only valid for function")
	error ("not finished")
end

--! Gets all operator functions, optionally filtered for the request \c op
function Ast:GetOperators (op)
	assert (self.arity == "byref" or self.arity == "byval" or self.arity == "bytie", ":GetOperators only valid for by[ref|val|tie]")
	assert (type (op) == "nil" or type (op) == "string")

	local found = {}
	for i, v in ipairs (ast.body) do
		if v.arity == "function" and ast:IsAnOperator ()
		and op and ast:GetName () == op then
			table.insert (found, ast)
		end
	end
	return found
end

--! Scans self for a decoration that matches the provided plain text name
--! This is the breadth first search, expected to be called by a supporting depth recuser
function Ast:ScanForNamedDelc (named)
	SmartPrintEnterLocation ("AST", "Enter ScanForNamedDelc ("..tostring (named)..")")

	assert (type (named) == "string", "arg 'named' should be string")
	assert (#named > 0, "invalid zero length string for name")

	local matches = {}

	if self.arity == "function" then

		for i, v in ipairs (self:GetBody ()) do
			if v.arity == "vardelc" then
				SmartPrintLocation ("AST", "potential "..tostring (v))
				if v:GetPath ():GetName ().text == named then
					SmartPrintLocation ("AST", "match "..tostring (v))
					table.insert (matches, v)
				end
			end
		end

		for i, v in ipairs (self:GetArguments ()) do
			if v.arity == "argument" then
				SmartPrintLocation ("AST", "potential "..tostring (v))
				if v:GetPath ():GetName ().text == named then
					SmartPrintLocation ("AST", "match "..tostring (v))
					table.insert (matches, v)
				end
			end
		end


	elseif self.arity == "namespace" or self.arity == "byval" or self.arity == "byref" or  self.arity == "bytie" then

		for i, v in ipairs (self:GetBody ()) do
			if v.arity == "vardelc" then
				SmartPrintLocation ("AST", "potential "..tostring (v))
				if v:GetPath ():GetName ().text == named then
					SmartPrintLocation ("AST", "match "..tostring (v))
					table.insert (matches, v)
				end
			end
		end

	elseif self.arity == "binary" or self.arity == "unary" or self.arity == "nonary" then
		-- skip expressions

	elseif self.arity == "return" or self.arity == "if" or self.arity == "for" then
		-- skip statements that don't allow vardelcs
		-- not that nested structutes that DO allow don't need to be tested
		-- as they would be found by the bredth first search expected by this call

	else
		error ("unhandled arity '"..self.arity.."' in ScanForNamedDelc")
	end



	SmartPrintLeaveLocation ("AST", "Leave ScanForNamedDelc ("..tostring (named)..") found "..#matches)
	return matches
end

-- AST CACHING

function Ast:GetCache (name)
	local c = self.cache
	if c then
		return c[name]
	end
	return nil
end

function Ast:SetCache (name, v)
	local c = self.cache
	if not c then
		c = {}
		rawset (self, "cache", c)
	end
	c[name] = v
end





----------------------------------------------------------------
--   A S T   L I S T
----------------------------------------------------------------
AstList, AstListMetatable = InheritFrom (AbstractAstBaseMetatable)

function AstList:ToShortString ()
	local r = "AstList#"..self.___uid.."{^#"..(self.parent and self.parent.___uid or "?")
	for i, v in ipairs (self) do
		print (luax.GetMetatableName (getmetatable (v)))
		r = r .. ", " .. v:ToShortString ()
	end
	return r.."}"
end

function AstList:ToString ()
	local r = "AstList#"..self.___uid.."{^#"..(self.parent and self.parent.___uid or "?")
	for i, v in ipairs (self) do
		r = r .. ", " .. tostring (v)
	end
	return r.."}"
end

function AstListMetatable:__tostring ()
	return self:ToShortString ()
end

function AstList.new ()
	return setmetatable ({___uid = Ast.___newuid ()}, AstListMetatable)
end

function AstList:AppendRaw (entry)
	rawset (self, #self + 1, entry)
end

--[[
-- Grammar notes
-- NOT EXAUSTIVE
-- (   )			grouping
-- ,				Sequence
-- |				Alternative
-- "literal"		matches source code directly (usually a keyword or punctuation)
-- <   >			matches one and only one instance of the contained rule
-- [   ]			optionally matches one instance of contained rule
-- {   }			matches one or more  instances of contained rule
-- ?				optionally matches something
----
-- "namespace", <path>, {body}, "end"
-- "byval", <path>, {body}?, "end"
-- "byref", <path>, {body}?, "end"
-- "bytie", <path>, {body}?, "end"
-- "for", <expr|vardelc>, ",", <expr>, ",", <expr>, <statement | ("do", {statement}?, "end")> (* C style *)
-- | "for", <expr|vardelc>, "->", <expr>, <statement | ("do", {statement}?, "end")> (* Range style *)
--
--
--
-- "+"|"-"|"*"|"/"|"%"|":"|"."|">="|"<="|"="|".."|"<-"|"<<" .... and so on
-- "+"|"-"|"#"|"~"|"!"
--
--
-- <"local"|"global">, <type>
-- ["local"|"global"]?, "function", <return types>, <path>, "(", <argument list>, ")", <body>, "end"
-- ["local"|"global"]?, "signature", <return types>, <path>, "(", <argument list>, ")"
]]


require "ast-dis"