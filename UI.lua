--[[ UI ]]

local UI = {}
UI.Metatable =
{__index = UI}

UI.Classes = {}
UI.Class_Names = {}

function UI.Load_class (name)
	assert (type(name) == "string"
	, "UI.Load_class expects a string")
	assert (UI.Classes[name] == nil
	, "[UI]   Requested class was already loaded: ".. name)
	local Class = assert (require ("UI/".. name)
	, "[UI]   Requested class not found: ".. name)
	table.insert (UI.Class_Names, name)
	setmetatable (Class, UI.Metatable)
	UI.Classes [name] = Class
	print ("[UI]   Loaded class: ".. name)
end

function UI.Load_core ()
	local Core_Classes =
	{	"Node"
	}
	for i = 1, #Core_Classes do
		UI.Load_class (Core_Classes [i])
	end
end

function UI.New (class, ...)
	return UI.Classes [class].New (...)
end

return UI
