--[[ 20180918 revisions

LCSS : graphical display and click detection
LLN : data structure and compilation
IO : user interaction layer

LCSS::
Absolute
Empty
Fill
Image
Ratio
Text
H/V Dividers
H/V Scrollers

LLN::
Definition
Instance
Namespace
Reference
System

IO::
Context
History

Make heavy use of get, set, and assert

For ".New" functions, only pass the minimum required fields
Non-mandatory fields should be controlled via methods
No direct setting of fields, not currently enforced but probably should be


Classes::

Namespace: Same as in lulu

System: A collection of interlinked nodes, analogous to a block of code

Instance: "Grey Box" representing a built in function or custom system, analogous
to a token

Definition: Contains data relavent to all instances of the same thing

Link: Describes a connection between two nodes



Structure::

Definitions

Namespace
	Definitions
	Systems
		Instances
		Links

Link



Namespace::
Editor : Declaritive grid

Definitions -> all node definitions belonging to the namespace
Systems -> all function systems belonging to the namespace



Definition::
Editor : Declaritive list

Inputs -> all input references
Outputs -> all output references
Build_hook -> hook function to build this node types path



System::
Editor : Flow graph

Nodes -> all nodes, ordered by compile preference
Links -> all links between nodes, undecided indexing
Subsystems -> all of the local systems (groups, if-statements, ect)



Instance::
Editor : Property tab

Definition -> Direct reference 
pos_x, pos_y -> The position of the center points
width, height -> Dimensions of the LCSS region
minimised -> Boolean indicating which LCSS should be used



Reference::
A table, to be used as an indexer, containing a string
References are the main indexing tool of LLN

name -> User friendly string
Link -> Another reference, who's name should be used should 

.New (name) -> Creates a new named reference
:Clone_linked -> Creates a new, unnamed reference that links to the reference
:__call () -> Resolves the reference name
:__tostring () -> """"
:Name_set (name) -> Sets the name of the reference
:Name_clear () -> Clears the name, asserts linked
:Link (Ref) -> Sets the linked reference
:Unlink () -> Clears the linked reference, inherits the name if no name is set
.Search (List, name) -> Searches List for references containing name

--]]
