

lln_ValuePin = {}
lln_ValuePin.Metatable =
{__index = lln_ValuePin}

lln_ValuePin.Defaults =
{	x = RSkyl.Required
,	y = RSkyl.Required
,	T = "any"
,	d = false
,	i = RSkyl.Required
}

lln_ValuePin.Mapping =
{	x = "x"
,	y = "y"
,	T = "T"
,	d = "documentation"
,	i = "id"
}

function lln_ValuePin.New (a)
	return	lln_MetaConstructor ( a
		,	lln_ValuePin)
end

function lln_ValuePin:Draw (w, T)
	local x =  w*self.x + 10
	local y = 20*self.y + 10
	
	RSkyl.SetColor (T, 0.5)
	love.graphics.rectangle('fill', x-6, y-6, 12, 12)
	
	RSkyl.SetColor (T)
	love.graphics.rectangle('fill', x-4, y-4,  8,  8)
end

function lln_ValuePin:Detect (x, y, w)
	return math.abs(x -  w*self.x - 10) <= 5
	   and math.abs(y - 20*self.y - 10) <= 5
end
