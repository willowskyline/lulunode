--[[ LLN ]]

local LLN = {}
LLN.Classes = {}
LLN.Class_Names = {}

function LLN.New (class_name, ...)
	return LLN.Classes [class_name].New (...)
end

function LLN.Load_class (name)
	assert (type(name) == type("")
	, "LLN.Load_class expects a string")
	assert (LLN.Classes[name] == nil
	, "[LLN]  Requested class was already loaded: ".. name)
	local Class = assert (require ("LLN/".. name)
	, "[LLN]  Requested class not found: ".. name)
	table.insert (LLN.Class_Names, name)
	LLN.Classes [name] = Class
	print ("[LLN]  Loaded class: ".. name)
end

function LLN.Load_core ()
	local Core_Classes =
	{	"Reference"
	,	"Definition"
	,	"Instance"
	,	"System"
	,	"Namespace"
	}
	for i = 1, #Core_Classes do
		LLN.Load_class (Core_Classes[i])
	end
end

return LLN
