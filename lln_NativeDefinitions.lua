

lln_Native = {}

function lln_Native.NewBinary (n, aT, bT, cT, H)
	return lln_Definition.New {n=n, w=30, h=2, H=H}
--[[bg    ]]:AddVisual	{x=0, y=0, h=2, b=2, a=0.5}
--[[title ]]:AddGrab	{}
			:AddTextBox	{x=0  , y=0, t=n, a="center"}--, r=10}
--[[ in a ]]:AddInPin	{x=0  , y=0, T=aT, i="a"}
--[[ in b ]]:AddVisual	{x=0  , y=1, w=0.5}
			:AddInPin	{x=0  , y=1, T=bT, i="b"}
--[[out c ]]:AddVisual	{x=0.5, y=1, w=0.5}
			:AddOutPin	{x=1  , y=1, T=cT, i="c"}
end

lln_Native.GeneralMath =
{"+","-","*","/","%","^"}

lln_Native.BitwiseOps =
{"|","&","~",">>","<<",">><","><<"}

lln_Native.Comparitors =
{"==","!=","<",">","<=",">="}

function lln_Native.Build ()
	--	general math
	for _,n in ipairs(lln_Native.GeneralMath) do
		lln_Native.NewBinary(n, "any", "any", "any"
		, lln_Hook.BinaryMatch)
	end
	--	bitwise ops
	for _,n in ipairs(lln_Native.BitwiseOps) do
		lln_Native.NewBinary(n, "int", "int", "int")
	end
	--	comparitors
	for _,n in ipairs(lln_Native.Comparitors) do
		lln_Native.NewBinary(n, "any", "any", "bool"
		, lln_Hook.BinaryComp)
	end
	lln_Native.NewBinary("is", "any", "tie", "bool")
	--	logic
	lln_Native.NewBinary("or" , "bool", "bool", "bool")
	lln_Native.NewBinary("and", "bool", "bool", "bool")
	--	misc
	lln_Native.NewBinary("[", "array", "int", "any")
	
	--	global array read index
	lln_Definition.New {w=60,h=3,n="g[i]->",s="read"}
		:AddStandardBackground ()
		:AddStandardGrab ("g[i]->")
		:AddVisual   {x=0,y=1}
		:AddInputBox {x=0,y=1,i="var"}
		:AddVisual   {x=0/3,y=2,w=1/3}
		:AddInPin    {x=0,y=2,i="i",T="int"}
		:AddVisual   {x=1/3,y=2,w=1/3,I="read"}
		:AddVisual   {x=2/3,y=2,w=1/3}
		:AddOutPin   {x=1,y=2,i="v"}
	
	--	global array write index
	lln_Definition.New {w=60,h=4,n="g[i]<-",s="write"}
		:AddStandardBackground ()
		:AddStandardGrab ("g[i]<-")
		:AddVisual   {x=0,y=1}
		:AddInputBox {x=0,y=1,i="var"}
		:AddVisual   {x=0,y=2,w=1/3}
		:AddInPin    {x=0,y=2,i="i",T="int"}
		:AddVisual   {x=0,y=3,w=1/3}
		:AddInPin    {x=0,y=3,i="v",T="any"}
		:AddVisual   {x=1/3,y=2,w=1/3,h=2,I="write"}
		:AddVisual   {x=2/3,y=2,w=1/3,h=2}
	
	--	TODO: remove this example function when testing is done
	lln_Definition.NewStandard ("GodFoo", "GodFoo"
	,	{	{i="a", t="alpha", T="float"}
		,	{i="b", t="beta", T="double"}
		}
	,	{	{i="r", t="result", T="string"}
		}
	,	false
	,	"wr"
	)
end
