local Reference = require ("LLN/Reference")
local LCSS = require ("LCSS")

local Node = {}
Node.Metatable =
{__index = Node}

Node.Color =
{	Base = {0.5,0.5,0.5,1.0}
,	Dark = {0.2,0.2,0.2,1.0}
}

function Node.Pin_new (id, col)
	return LCSS.New ("Absolute",
	{	width = 12
	,	height = 12
	,	LCSS.New ("Fill",
		{	id = id
		,	rgba = col
		,	alpha = 0.5
		,	LCSS.New ("Fill",
			{	rgba = col
			,	padding = 2
			})
		})
	})
end

function Node.Title_new (id, text)
	return LCSS.New ("Cut",
	{	id = id
	,	padding = 1
	,	rgba = {0.5,0.5,0.5,1.0}
	,	LCSS.New ("Text",
		{	text = text
		})
	})
end

function Node.Input_new (id, text)
	return LCSS.New ("Fill",
	{	padding = 1
	,	rgba = Node.Color.Base
	,	LCSS.New ("Fill",
		{	id = id
		,	padding = 2
		,	rgba = Node.Color.Dark
		,	LCSS.New ("Text",
			{	text = text
			})
		})
	})
end

local function Pattern (T, n, N)
	if n ~= N then
		table.insert (T,
		LCSS.New ("Fill",
		{padding = 1}))
		table.insert (T, 1,
		LCSS.New ("Fill",
		{padding = 1}))

		local spare = (N-n)/2
		T.Proportion =
		{	[ 1 ] = spare
		,	[n+2] = spare
		}
	end
	return LCSS.New ("VDivider", T)
end

function Node.New (title, In, Out)
	local n_in  = #In
	local n_out = #Out
	local n_max = math.max (n_in, n_out)

	In  = Pattern (In , n_in , n_max)
	Out = Pattern (Out, n_out, n_max)

	local Body =
	{	Dispute = {20,0}
	,	Proportion = {0,1}
	,	Node.Title_new ("grab", title)
	}
	local Pins_In =
	{	Dispute = {}
	}
	local Pins_Out =
	{
	}

	return LCSS.New ("Empty",
	{	LCSS.New ("HDivider",
		{	Dispute = {10,00,10}
		,	Proportion = {0,1,0}
		,	LCSS.New ("Empty", {})
		,	LCSS.New ("VDivider", Body)
		,	LCSS.New ("Empty", {})
		})
	,	LCSS.New ("HDivider",
		{	Dispute = {20,00,20}
		,	Proportion = {0,1,0}
		,	LCSS.New ("VDivider", Pins_In)
		,	LCSS.New ("Empty", {})
		,	LCSS.New ("VDivider", Pins_Out)
		})
	})
end

return Node
