--require "utf8"
require "RSkyl"
require "lln_System"
require "lln_User"

lln_User.system = lln_System.New ("main"
,	{{i="argc",t="argc",T="int"},{i="argv",t="argv",T="array"}}
,	{{i="exit",t="exit",T="int"}}
)

griddots = {}

function love.load()
	love.graphics.setLineWidth(2)
	
	InitTypeGraphics ()
	
	lln_Native.Build ()
	
	lln_User.LoadCursors ()
	love.mouse.setCursor (lln_User.cursors.default)
	
	local src_def = lln_User.system.source.definition
	local src_pin = src_def:GetByID "argc"
	
	local sink_def = lln_User.system.sink.definition
	local sink_pin = sink_def:GetByID "exit"
	
	local ioref = lln_User.system.sink.ioref[sink_pin]
	ioref.node = lln_User.system.source
	ioref.pin = src_pin
	
	lln_User.system:AddInstance {d=lln_Definition.List["g[i]->"], x=200, y=100}
	lln_User.system:AddInstance {d=lln_Definition.List["g[i]<-"], x=260, y=180}
	lln_User.system:AddInstance {d=lln_Definition.List["+"], x=320, y=260}
	lln_User.system:AddInstance {d=lln_Definition.List["-"], x=360, y=260}
	lln_User.system:AddInstance {d=lln_Definition.List["*"], x=320, y=300}
	lln_User.system:AddInstance {d=lln_Definition.List["/"], x=360, y=300}
	lln_User.system:AddInstance {d=lln_Definition.List["GodFoo"], x=500, y=300}
	
	for x=0,800,10 do
		for y=0,600,10 do
			table.insert(griddots,x)
			table.insert(griddots,y)
		end
	end
	love.graphics.setPointSize (2)
end

function love.update(dt)
	lln_User.hook ()
end

function love.draw ()
	
	local mx, my = love.mouse.getPosition()
	
	love.graphics.clear (0.08,0.16,0.12)
	
	RSkyl.SetColor "White"
	love.graphics.print (string.format("m:(%03i,%03i)", mx, my))
	
	RSkyl.SetColor (0.16,0.32,0.24)
	if lln_User.snap then
		love.graphics.points (griddots)
	end
	
	lln_User.system:Draw (0,0)
	
	if lln_User.hand and not lln_User.islinking then
		
	else
		local res = lln_User.system:Detect (mx,my)
		if res then
			love.mouse.setCursor (lln_User.cursors[res.sub])
		else
			love.mouse.setCursor (lln_User.cursors.default)
		end
	end
	if lln_User.islinking then
		love.graphics.line (mx, my, lln_User.px, lln_User.py)
	end
end

