--[[ IO ]]

local IO = {}
IO.Classes = {}
IO.Class_Names = {}

function IO.Load_class (name)
	assert (type(name) == type("")
	, "IO.Load_class expects a string")
	assert (IO.Classes[name] == nil
	, "[IO]   Requested class was already loaded")
	local Class = assert (require ("IO/".. name)
	, "[IO]   Requested class not found: ".. name)
	table.insert (IO.Class_Names, name)
	IO.Classes [name] = Class
	print ("[IO]   Loaded class: ".. name)
end

function IO.Load_core ()
	local Core_Classes =
	{	"Path"
	,	"Context"
	}
	for i = 1, #Core_Classes do
		IO.Load_class (Core_Classes[i])
	end
end

return IO
