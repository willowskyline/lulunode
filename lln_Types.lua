ColorTable =
{	Grey   = {0.5,0.5,0.5}
,	White  = {1.0,1.0,1.0}
,	Dark   = {0.2,0.2,0.2}

,	Sync   = {0.0,1.0,0.5}
,	flow   = {1.0,0.5,0.4}
,	read   = {0.0,1.0,1.0}
,	write  = {0.0,1.0,0.5}
,	wr     = {1.0,0.8,0.0}
,	Warn   = {1.0,0.2,0.1}

,	bool   = {0.8,0.2,0.2}
,	char   = {1.0,0.4,0.0}
,	string = {1.0,0.8,0.0}
,	int    = {0.4,0.8,0.2}
,	float  = {0.2,0.8,0.7}
,	double = {0.6,0.4,0.8}
,	array  = {0.9,0.5,0.7}
,	any    = {1.0,1.0,1.0}
}

TypePrecidence =
{	"bool"
,	"int"
,	"float"
,	"double"
}

SyncIcons = {}

function InitTypeGraphics ()
	local icons = {"read", "write", "wr", "flow"}
	for i,v in ipairs(icons) do
		SyncIcons[v] = love.graphics.newImage ("icons/"..v..".png")
	end
end

lln_Type = {}

lln_Type.Colors = 
{	any    = {1.0,1.0,1.0}
,	bool   = {0.8,0.2,0.2}
,	char   = {1.0,0.4,0.0}
,	string = {1.0,0.8,0.0}
,	int    = {0.4,0.8,0.2}
,	float  = {0.2,0.8,0.7}
,	double = {0.6,0.4,0.8}
,	array  = {0.9,0.5,0.7}
}

function lln_Type.GetColor (t)
	if type(t) == type{} then t = "array" end
	return lln_Type.Colors [t]
end


