

lln_Visual = {}
lln_Visual.Metatable =
{__index = lln_Visual}

lln_Visual.Defaults =
{	x = RSkyl.Required
,	y = RSkyl.Required
,	w = 1.0
,	h = 1.0
,	b = 1
,	a = 1.0
,	I = false
}

lln_Visual.Mapping =
{	x = "x"
,	y = "y"
,	w = "width"
,	h = "height"
,	b = "border"
,	a = "alpha"
,	I = "icon"
}

function lln_Visual.New (a)
	return	lln_MetaConstructor ( a
		,	lln_Visual)
end

function lln_Visual:Draw (w)
	RSkyl.SetColor ("Grey", self.alpha)
	love.graphics.rectangle('fill'
	,	 w*self.x + self.border + 10
	, 	20*self.y + self.border
	,	 w*self.width  - self.border*2
	,	20*self.height - self.border*2
	)
	if self.icon then
		RSkyl.SetColor ("White")
		local icon = SyncIcons[self.icon]
		local iw, ih = icon:getDimensions ()
		love.graphics.draw ( icon
		,	 w*self.x + 0.5*w*self.width + 10
		,	20*self.y +    10*self.height
		,	0
		,	1
		,	1
		,	0.5*iw
		,	0.5*ih
		)
	end
end

function lln_Visual:Detect (x,y,w)
	local lx = 10 +  w*self.x
   	local rx = lx +  w*self.width
   	local ty =      20*self.y
   	local by = ty + 20*self.height
	
	lx = lx + self.border
	rx = rx - self.border
	ty = ty + self.border
	by = by - self.border
	
	return lx < x and x < rx
	   and ty < y and y < by
end
