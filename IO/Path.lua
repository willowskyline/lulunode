local Node = {}
Node.Metatable =
{__index = Node}

function Node.New (key, sub)
	return setmetatable ({key = key, sub = sub}, Node.Metatable)
end

function Node:Index (Object)
	return Object [key][sub]
end



local Path = {}
Path.Metatable =
{__index = Path}
Path.Node = Node

function Path.New (Root, ...)
	local List = {...}
	for i,n in ipairs (List) do
		assert (getmetatable (n) == Node.Metatable
		, "Path must be made of path nodes, error on argument ".. i)
	end
	
	setmetatable (List, Path.Metatable)
	List.Root = Root
	List:Resolve()

	return List
end

function Path:Back (n)
	n = math.min (n or 1, #self)
	while n > 0 do
		table.remove (self, #self)
		n = n - 1
	end
	return self:Resolve()
end

function Path:Append (Path_Node)
	table.insert (self, Path_Node)
	return self:Resolve()
end

function Path:Resolve ()
	local Object = LLN

	for i = 1, #self do
		local Path_Node = self[i]
		Object = Path_Node:Index (Object)
	end
	self.End = Object

	return self
end

return Path
