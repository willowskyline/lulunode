local Path = require ("IO/Path")

local Context = {}
Context.Metatable =
{__index = Context}

function Context.New (Path)
	return setmetatable ({Path = Path}, Context.Metatable)
end

return Context
