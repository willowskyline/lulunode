

lln_Grab = {}
lln_Grab.Metatable =
{__index = lln_Grab}

lln_Grab.Defaults =
{	x = 0.0
,	y = 0.0
,	w = 1.0
,	h = 1.0
}

lln_Grab.Mapping =
{	x = "x"
,	y = "y"
,	w = "width"
,	h = "height"
}

function lln_Grab.New (a)
	return	lln_MetaConstructor ( a
		,	lln_Grab)
end

function lln_Grab:Draw (w)
	local lx = 10 +  w*self.x
	local rx = lx +  w*self.width
	local ty =      20*self.y
	local by = ty + 20*self.height
	
	RSkyl.SetColor ("Grey")
	love.graphics.polygon('fill'
	,	lx +  1,   ty +  1
	,	lx +  1,   by -  1
	,	rx -  1,   by -  1
	,	rx -  1,   ty + 10
	,	rx - 10,   ty +  1
	)
end

function lln_Grab:Detect (x,y,w)
	local lx = 10 +  w*self.x
   	local rx = lx +  w*self.width
   	local ty =      20*self.y
   	local by = ty + 20*self.height
	
	return lx < x and x < rx
	   and ty < y and y < by
	   and (x-y + ty-rx < -15	--	cut corner for resize
		   and "grab" or "resize")
end
