

lln_InputBox = {}
lln_InputBox.Metatable =
{__index = lln_InputBox}

lln_InputBox.Defaults =
{	x = RSkyl.Required
,	y = RSkyl.Required
,	w = 1.0
,	l = 0
,	r = 0
,	t = "nil"
,	d = false
,	i = RSkyl.Required
}

lln_InputBox.Mapping =
{	x = "x"
,	y = "y"
,	w = "width"
,	l = "left"
,	r = "right"
,	t = "text"
,	d = "documentation"
,	i = "id"
}

function lln_InputBox.New (a)
	return	lln_MetaConstructor ( a
		,	lln_InputBox)
end

function lln_InputBox:Draw (w, t)
	local x =  w*self.x + 10 + self.left + 3
	local y = 20*self.y + 3
	local width = w*self.width - (self.left + self.right) - 6
	
	RSkyl.SetColor ("Dark")
	love.graphics.rectangle ('fill'
	,	x
	,	y
	,	width
	,	14
	)
	
	RSkyl.SetColor ("White")
	love.graphics.printf ( t
	,	x + 5
	,	y
	,	width - 10
	,	"left"
	)
end

function lln_InputBox:Detect (x, y, w)
	local l = w*self.x + self.left + 10
	local t = 20*self.y + 3
	local r = w*self.width - (self.left + self.right) + l
	local b = t + 14
	
	return l<=x and x<=r and t<=y and y<=b
end
