require "lln_Definition"
require "lln_NativeDefinitions"
require "lln_NodeReference"


lln_Instance = {}
lln_Instance.Metatable =
{__index = lln_Instance}

lln_Instance.Defaults =
{	d = RSkyl.Required
,	x = RSkyl.Required
,	y = RSkyl.Required
}

lln_Instance.Mapping =
{	d = "definition"
,	x = "x"
,	y = "y"
}

function lln_Instance.New (a)
	local new = lln_MetaConstructor ( a
			,	lln_Instance)
	new.width  = new.definition.width
	new.height = new.definition.height * 20
	new.ioref  = new.definition:GenerateReferences()
	new.canvas = false
	return new
end

function lln_Instance:UpdateCanvas ()
	if self.canvas then
		self.canvas:release()
	end
	self.canvas =
	love.graphics.newCanvas (20+self.width, self.height)
	love.graphics.setCanvas (self.canvas)
	self.definition:Draw (self.width, self.ioref)
	love.graphics.setCanvas ()
end

function lln_Instance:DrawConnections (x, y)
	local def = self.definition
	for _,p in ipairs(def.Component.PinIn) do
		local ref = self.ioref[p]
		if ref.node then
			local srcx = 10 - x + ref.node.x + ref.pin.x*ref.node.width
			local srcy = 10 - y + ref.node.y + ref.pin.y*20
			local dstx = 10 - x +     self.x +       p.x*self.width
			local dsty = 10 - y +     self.y +       p.y*20
			
			love.graphics.line (srcx, srcy, dstx, dsty)
		end
	end
	if def.sync and false then
		local x = self.x + (self.width)*0.5 + 10
		love.graphics.line (x,0,x,2000)
	end
end

function lln_Instance:Draw (x, y)
	--[[
	love.graphics.rectangle( 'line'
	,	self.x - x - 2
	,	self.y - y - 2
	,	self.width + 24
	,	self.height + 4
	)
	--]]
	love.graphics.draw ( self.canvas
	,	self.x - x
	,	self.y - y
	)
end

function lln_Instance:Detect (x, y)
	if  self.x <= x and x <= self.x + self.width + 20
	and self.y <= y and y <= self.y + self.height
	then
		local res = self.definition:Detect (x - self.x, y - self.y, self.width)
		if res then
			res.node = self
		end
		return res
	end
end

function lln_Instance:GetPinPosition (p)
	local x = self.x + self.width*p.x + 10
	local y = self.y +         20*p.y + 10
	return x, y
end

function lln_Instance:DoHook()
	return self.definition.hook (self)
end

function lln_Instance:GenPinName (pin)
	return "T".. tostring(self):match"0x(%x*)"
		.. "P".. tostring(pin ):match"0x(%x*)"
end

--function lln_Instance:LinkWith (sp, dn, dp)



