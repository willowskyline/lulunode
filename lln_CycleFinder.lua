lln_CycleFinder = {}

function lln_CycleFinder.CheckFor (node, targs)
	for _,p in ipairs(node.definition.Component.PinIn) do
		local ref = node.ioref [p]
		if ref.node then
			if RSkyl.IsIn (targs, ref.node)
			or lln_CycleFinder.CheckFor (ref.node, targs)
			then return true end
		end
	end
	return false
end

function lln_CycleFinder.InsertionCheck (node)
	return lln_CycleFinder.CheckFor (node, {node})
end


