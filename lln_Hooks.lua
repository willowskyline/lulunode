
lln_Hook = {}

function lln_Hook.None (inst)
	return true
end

function lln_Hook.BinaryMatch (inst)
	local def = inst.definition
	local   a = def:GetByID "a"
	local   b = def:GetByID "b"
	local   c = def:GetByID "c"
	local ref = inst.ioref
	local  aT = ref[a]:ResolveType()
	local  bT = ref[b]:ResolveType()
	
	if aT == bT or bT == "any" then
		ref[a].T = aT
		ref[b].T = aT
		ref[c]   = aT
	elseif aT == "any" then
		ref[a].T = bT
		ref[b].T = bT
		ref[c]   = bT
	else
		return false
	end
	return true
end

function lln_Hook.BinaryComp (inst)
	local def = inst.definition
	local   a = def:GetByID "a"
	local   b = def:GetByID "b"
	local ref = inst.ioref
	local  aT = ref[a]:ResolveType()
	local  bT = ref[b]:ResolveType()
	
	if aT == "any" then
		ref[a].T = bT
		ref[b].T = bT
		return true
	else
		if bT == "any" or aT == bT then
			ref[a].T = aT
			ref[b].T = aT
			return true
		else
			return false
		end
	end
end
