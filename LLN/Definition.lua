local Reference = require ("LLN/Reference")

local Definition = {}
Definition.Metatable =
{__index = Definition}

--	All definitions go here, indexed by name references
Definition.List = {}
Definition.Reference_List = {}

function Definition.New (name)
	local New =
	{	Reference = Reference.New (name)
	,	Inputs = {}
	,	Outputs = {}
	,	Meta_Data = {}
	,	Compile_hook = false
	}

	table.insert (Definition.Reference_List, New.Reference)
	Definition.List [New.Reference] = New
	return New
end

function Definition:Input_add (Reference, Type)
	self.Inputs [Reference] = Type
end

function Definition:Compile_hook_set (Hook_new)
	self.Build_hook = Hook_new
	return self
end

function Definition:Compile (Instance_Data)
	return self.Compile_hook (self, Instance_Data)
end

return Definition
