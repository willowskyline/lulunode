local Reference = require ("LLN/Reference")
local Definition = require ("LLN/Definition")
local System = require ("LLN/System")

local Namespace = {}
Namespace.Metatable =
{__index = Namespace}

function Namespace.New (name)
	New =
	{	Reference = Reference.New (name)
	,	Definitions = {}
	,	Systems = {}
	}
	return setmetatable (New, Namespace.Metatable)
end

return Namespace
