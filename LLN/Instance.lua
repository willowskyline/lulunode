local Reference = require ("LLN/Reference")
local Definition = require ("LLN/Definition")
local UIElement = require ("UI/Node")

local Instance = {}
Instance.Metatable =
{__index = Instance}

function Instance.New (Definition, pos_x, pos_y)
	local New =
	{	Reference = Definition.Reference:Clone_linked ()
	,	Definition = Definition
	,	pos_x = pos_x
	,	pos_y = pos_y
	,	UIElement = false
	}
	return New
end

function Instance:Get_pos ()
	return self.pos_x, self.pos_y
end

function Instance:Set_pos (pos_x, pos_y)
	self.pos_x = pos_x
	self.pos_y = pos_y
	return self
end

function Instance:Draw (offset_x, offset_y)
	
end

return Instance
