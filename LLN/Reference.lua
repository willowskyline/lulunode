local Reference = {}
Reference.Metatable =
{__index = Reference}

function Reference.New (name)
	assert (type (name) == type ("")
	, "Expected name to be string, got: ".. type (name))

	return setmetatable ({name = name, Link = false}, Reference.Metatable)
end

function Reference:Clone_linked ()
	return setmetatable ({name = false, Link = self}, Reference.Metatable)
end

function Reference.Metatable:__call ()
	return self.name
	or assert (self.Link()
	, "Attempted to get the name of a nameless unlinked reference")
end
Reference.Metatable.__tostring = Reference.Metatable.__call

function Reference:Name_set (name)
	assert (type(name) == type("")
	, "Expected name to be string, got: "..type(name))

	self.name = name
	return self
end

function Reference:Name_clear ()
	assert (self.Link
	, "Attempted to clear the name of an unlinked reference")

	self.name = false
	return self
end

function Reference:Link (Ref)
	assert (getmetatable(Ref) == Reference.Metatable
	, "Reference must link to another reference")

	self.Link = Ref
	return self
end

function Reference:Unlink ()
	self.name = self()
	self.Link = false
	return self
end

local function Order (El_A, El_B)
	return El_A.v < El_B.v
end

function Reference.Search (List, string)
	string = string:lower()
	local Result = {}
	for i = 1, #List do
		local Ref = List[i]
		local name = Ref.name:lower()
		local val = name:find (string)
		if val then
			table.insert (Result, {R = Ref, v = val})
		end
	end

	table.sort (Result, Order)

	for i = 1, #Result do
		Result [i] = Result[i].R
	end
	return Result
end

return Reference
