local Reference = require ("LLN/Reference")
local Definition = require ("LLN/Definition")
local Instance = require ("LLN/Instance")

local System = {}
System.Metatable =
{__index = System}

function System.New ()
	local New =
	{	Nodes = {}
	,	Links = {}
	,	Subsystems = {}
	}
	return setmetatable (New, System.Metatable)
end

local function Order (Node_A, Node_B)
	if Node_A.pos_x == Node_B.pos_x then
		return Node_A.pos_y < Node_B.pos_y
	end
	return Node_A.pos_x < Node_B.pox_x
end

function System:Sort ()
	table.sort (self.Nodes, Order)
	return self
end

return System
