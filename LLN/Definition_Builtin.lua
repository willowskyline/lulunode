local Def = require ("Definition")

local function Compile_inflix (Def_Data, Inst_Data)
	local op = Def_Data.Reference ()
	return op
end

return function ()
	--	plain math
	Definition.New ("+")
	:Compile_hook_set (Compile_inflix)
end
