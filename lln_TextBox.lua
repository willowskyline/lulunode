

lln_TextBox = {}
lln_TextBox.Metatable =
{__index = lln_TextBox}

lln_TextBox.Defaults =
{	x = RSkyl.Required
,	y = RSkyl.Required
,	w = 1.0
,	l = 0
,	r = 0
,	t = RSkyl.Required
,	a = "left"
,	i = false
}

lln_TextBox.Mapping =
{	x = "x"
,	y = "y"
,	w = "width"
,	l = "left"
,	r = "right"
,	t = "text"
,	a = "align"
,	i = "id"
}

function lln_TextBox.New (a)
	return	lln_MetaConstructor ( a
		,	lln_TextBox)
end

function lln_TextBox:Draw (w, t)
	RSkyl.SetColor ("White")
	love.graphics.printf ( t or self.text
	,	15 +  w*self.x + self.left
	,	 3 + 20*self.y
	,	w*self.width - (10 + self.left + self.right)
	,	self.align
	)
end
