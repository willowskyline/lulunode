-- Front end, Front end

Token = {}
TokenMetatable = { __index = Token }

function TokenMetatable:__eq (other)
	error ("Token comparison "..luax.ToStringWithType (self).." == "..luax.ToStringWithType (other))
end
function TokenMetatable:__neq (other)
	error ("Token comparison "..luax.ToStringWithType (self).." == "..luax.ToStringWithType (other))
end
function TokenMetatable:__lt (other)
	error ("Token comparison "..luax.ToStringWithType (self).." < "..luax.ToStringWithType (other))
end
function TokenMetatable:__le (other)
	error ("Token comparison "..luax.ToStringWithType (self).." <= "..luax.ToStringWithType (other))
end

function TokenMetatable:__tostring ()
	return string.format ("Token{`%s`@%s}", self.text, self:LocationPretty ())
end
function TokenMetatable:__newindex (key, val)
	error ("Cannot set key "..ToStringWithType (index).. " to value "..ToStringWithType (value))
end
function TokenMetatable.__eq (a, b)
	error ("Compare Token with something: "..ToStringWithType (a).. "=="..ToStringWithType (b))
end

function Token.new (str, file, line, column)
	line = line or -1
	column = column or -1
	return setmetatable ({text = str, file = file, line = line, column = column}, TokenMetatable)
end

function Token:LocationPretty ()
	if self.line == -1 and self.column == -1 then
		return string.format ("phantom", self.sourcefile)
	end
	return string.format ("%s:%u:%u", self.sourcefile, self.line, self.column)
end
function Token:ToShortString ()
	return tostring (self)
end
function Token:ToString ()
	return tostring (self)
end






Tokeniser = {}
TokeniserMetatable = {  __index = Tokeniser }

function TokeniserMetatable:__tostring ()
	return string.format ("Tokeniser{line=%u column=%u sourcefile=`%s` singleChevrons=%s Current=%s}", self.line, self.column, self.sourcefile, tostring (self.singleChevrons), tostring (self.Current))
end

function --[[Tokeniser]] Tokeniser.new (--[[string]] filename)
	luax.assertarg (filename, "filename", "string")
	
	local f = io.open (filename, "rb")
	if f == nil then
		io.stderr:write ("Failed to open file '", filename, "'; stop\n")
		os.exit (1)
	end
	local t = setmetatable ({singleChevrons = false, file = f, putback = {}, nextToken = nil, line = 1, column = 1, sourcefile = filename}, TokeniserMetatable)
	
	return t
end

function Tokeniser:Expect (--[[string or table<string>]] text)
	
	local switch = type (text)
	if switch == "table" then
		for i, v in ipairs (text) do
			if self.Current.text == v then
				return self:Advance ()
			end
		end
		io.stderr:write ("[TOKEN] Expected {'", table.concat (text, "', '"), "'} got '", self.Current.text, "'; stop\n")
		dbgHook ()
		os.exit (1)

	elseif switch == "string" then		
		if self.Current.text ~= text then
			io.stderr:write ("[TOKEN] Expected '", text, "' got '", self.Current.text, "'; stop\n")
			dbgHook ()
			os.exit (1)
		end
		return self:Advance ()
	else
		error ("Tokeniser:Expect (string or table<string> got "..switch)
	end
end

function --[[char]] Tokeniser:GetChar (--[[void]])
	if #self.putback == 0 then				-- if no undone chars
		return self.file:read (1)			-- read new
	else
		return table.remove (self.putback)	-- else pop from FILO
	end
end

function --[[void]] Tokeniser:PutChar (--[[char]] c)
	table.insert (self.putback, c)
end

-- REWORK TO USE TOKEN QUEUE SO THAT MULTIPLE TOKENS CAN BE PUSHED

-- Caller is expected to correctly set self.column as they process each char
function Tokeniser:MkToken0or1 (--[[string]] word)
	if word == nil or #word == 0 then
		self.Current = nil
		return nil
	end
	
	local t = setmetatable ({text = word, line = self.line, column = self.column - #word, sourcefile = self.sourcefile}, TokenMetatable)
	
	self.Current = t
	SmartPrintLocation ("TOKEN", "Current"..tostring (self.Current))
	return t
end
-- Makes a phantom token (one that does not realy exist)
function Tokeniser:MkTokenPhantom (line, column, txt)
	local t = setmetatable ({line = line, column = column, sourcefile = self.sourcefile, text = txt}, TokenMetatable)
	SmartPrintLocation ("TOKEN", "Phantom"..tostring (t))
	return t
end
-- Caller is expected to correctly set self.column upto the operator as they process each char, this function then advances self.column for the operator
function Tokeniser:MkToken1or2 (--[[string]] word, --[[string]] oper)
	if word == nil or #word == 0 then
		self.Current = setmetatable ({text = oper, line = self.line, column = self.column - #word, sourcefile = self.sourcefile}, TokenMetatable)
	else
		self.Current = setmetatable ({text = word, line = self.line, column = self.column - #word, sourcefile = self.sourcefile}, TokenMetatable)
		self.nextToken = setmetatable ({text = oper, line = self.line, column = self.column, sourcefile = self.sourcefile}, TokenMetatable)
	end
	self.column = self.column + #oper
	SmartPrintLocation ("TOKEN", "Current"..tostring (self.Current))
	return self.Current
end

function Tokeniser:SingleChevrons (--[[[bool]] enforce)
	self.singleChevrons = enforce
end

function Tokeniser:PushTokenBack (token)
	assert (self.nextToken == nil, "PushToken overflow")
	assert (getmetatable (token) == TokenMetatable, "'"..luax.ToStringWithType(token).." is not a token")
	self.nextToken = self.Current
	self.Current = token
end

function Tokeniser:PushToken (token)
	assert (self.nextToken == nil, "PushToken overflow")
	assert (getmetatable (token) == TokenMetatable, "'"..luax.ToStringWithType(token).." is not a token")
	self.nextToken = self.Current
	self.Current = token
end

function --[[Token]] Tokeniser:Advance (--[[void]])
		
	if self.nextToken ~= nil then
		self.Current = self.nextToken
		self.nextToken = nil
		SmartPrintLocation ("TOKEN", "Current"..tostring (self.Current))
		return self.Current
	end
	
	
	local word = ""
	
	while true do
	::continue::
		local c = self:GetChar ()
		
	--	print (word, c)
		
		if c == nil then
			return (self:MkToken0or1 (word)) -- return what remains, or nil of word is empty
		end
		
		-- check for no emit seperators
		
		if c == " " then
			local t = self:MkToken0or1 (word)
			self.column = self.column + 1
			if t ~= nil then return t end	-- something followed by space
			-- nothing followed by space, no concat & loop
		
		elseif c == "\t" then
			local t = self:MkToken0or1 (word)
			self.column = self.column + 4	-- assume 4 spaces
			if t ~= nil then return t end	-- something followed by space
			-- nothing followed by space, no concat & loop
		
		elseif c == "\n" then
			local t = self:MkToken0or1 (word)
			self.column = 1
			self.line = self.line + 1
			if t ~= nil then return t end	-- something followed by newline
			-- nothing followed by newline, no concat & loop
				
		-- check operators and delimiters
		
		elseif c == '"' then -- short string
			local str = c
			local escaped = false
			
			while true do
				c = self:GetChar ()
				str = str .. c
				if c == "\\" then
					escaped = true
				elseif c == '"' and not escaped then
					break
				end
			end
			return self:MkToken1or2 (word, str)
		
		elseif c == "/" then
			local c = self:GetChar ()
			if c == "/" then	-- line comment
				while self:GetChar () ~= "\n" do
				end
				self.line = self.line + 1
				self.column = 1
				SmartPrintLocation ("TOKEN", "Line comment")
				goto continue

			elseif c == "[" then -- long comment
				-- count number of opening '['
				local start_column = self.column
				local start_line = self.line
				local n = 1
				while true do
					c = self:GetChar ()
					if c == "[" then
						n = n + 1
					else
						break
					end
				end
				
				if c == '\n' then
					self.line = self.line + 1
					self.column = 1
				elseif c == '\t' then
					self.column = self.column + 4 + n
				else
					self.column = self.column + 1 + n
				end
				
				local m = "~"..("]"):rep (n).."/" -- closing pattern
				local p = "~~"
				
				repeat
					local c = self:GetChar ()
					
					if c == '\n' then
						self.line = self.line + 1
						self.column = 1
					elseif c == '\t' then
						self.column = self.column + 4
					elseif c == nil then
						io.write ("Long comment starting '/", ("["):rep (n), "' in ", self.sourcefile, ":", start_line, ":", start_column, " was not closed within sourcefile\n")
						os.exit (1)
					else
						self.column = self.column + 1
					end
					
					if c ~= "]" and c ~= "/" then
						c = "~"
					end
					p = p..c
					p = p:sub (-m:len ())
					
				until p == m
				
				SmartPrintLocation ("TOKEN", "Long comment ending '", m, "'\n")
				goto continue
				
			elseif c == "=" then	-- /=
				return self:MkToken1or2 (word, "/=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "/")
			end
			
			
		
		elseif c == ":" then
			local c = self:GetChar ()
			if c == ">" then
				return self:MkToken1or2 (word, ":>")
			elseif c == ":" then
				return self:MkToken1or2 (word, "::")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, ":")
			end	
				
		elseif c == "+" then
			local c = self:GetChar ()
			if c == "+" then
				return self:MkToken1or2 (word, "++")
			elseif c == "=" then
				return self:MkToken1or2 (word, "+=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "+")
			end	
				
		elseif c == "-" then
			local c = self:GetChar ()
			if c == "-" then
				return self:MkToken1or2 (word, "--")
			elseif c == "=" then
				return self:MkToken1or2 (word, "-=")
			elseif c == ">" then
				return self:MkToken1or2 (word, "->")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "-")
			end	
				
		elseif c == "*" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "*=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "*")
			end	
				
		elseif c == "^" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "^=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "^")
			end	
				
		elseif c == "&" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "&=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "&")
			end	
				
		elseif c == "|" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "|=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "|")
			end	
				
		elseif c == "=" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "==")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "=")
			end	
		
		elseif c == "~" then
			local c = self:GetChar ()
			if c == "=" then
				return self:MkToken1or2 (word, "~=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "~")
			end	
				
		elseif c == "." then
			local c = self:GetChar ()
			if c == "." then
				local c = self:GetChar ()
				if c == "=" then
					return self:MkToken1or2 (word, "..=")
				else
					self:PutChar (c)
					return self:MkToken1or2 (word, "..")
				end
			else
				-- check if word starts with a digit and c is a digit: num.digit
				if word:match ("^%d") and not word:match ("^%.") then
					if c:match ("^%d") then
						-- concat to make a longer word
						word = word .. "." .. c
						self.column = self.column + 2
					end
				else
					self:PutChar (c)
					return self:MkToken1or2 (word, ".")
				end
			end	
				
		elseif c == "," then
			return self:MkToken1or2 (word, ",")
				
		elseif c == ";" then
			return self:MkToken1or2 (word, ";")
				
		elseif c == "(" then
			return self:MkToken1or2 (word, "(")
				
		elseif c == ")" then
			return self:MkToken1or2 (word, ")")
				
		elseif c == "[" then
			return self:MkToken1or2 (word, "[")
				
		elseif c == "]" then
			local c = self:GetChar ()
			if c == ">" then
				return self:MkToken1or2 (word, "]>")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "]")
			end
				
		elseif c == "{" then
			return self:MkToken1or2 (word, "{")
				
		elseif c == "}" then
			return self:MkToken1or2 (word, "}")
				
		elseif c == "#" then
			return self:MkToken1or2 (word, "#")
		
		elseif self.singleChevrons and c == ">" then
			return self:MkToken1or2 (word, ">")
		
		elseif c == ">" then
			assert (not self.singleChevrons)
			
			local c = self:GetChar ()
			if c == ">" then
				local c = self:GetChar ()
				if c == "=" then
					return self:MkToken1or2 (word, ">>=")
				elseif c == "<" then
					local c = self:GetChar ()
					if c == "=" then
						return self:MkToken1or2 (word, ">><=")
					else
						self:PutChar (c)
						return self:MkToken1or2 (word, ">><")
					end
				else
					self:PutChar (c)
					return self:MkToken1or2 (word, ">>")
				end
			elseif c == "=" then
				return self:MkToken1or2 (word, ">=")
			elseif c == "<" then
				local c = self:GetChar ()
				if c == "<" then
					local c = self:GetChar ()
					if c == "=" then
						return self:MkToken1or2 (word, "><<=")
					else
						self:PutChar (c)
						return self:MkToken1or2 (word, "><<")
					end
				else
					self:PutChar (c)
				end
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, ">")
			end	
				
		elseif c == "<" then
			local c = self:GetChar ()
			if c == "[" then
				return self:MkToken1or2 (word, "<[")
			elseif c == "<" then
				local c = self:GetChar ()
				if c == "=" then
					return self:MkToken1or2 (word, "<<=")
				else
					self:PutChar (c)
					return self:MkToken1or2 (word, "<<")
				end
			elseif c == "-" then
				return self:MkToken1or2 (word, "<-")
			elseif c == "=" then
				return self:MkToken1or2 (word, "<=")
			else
				self:PutChar (c)
				return self:MkToken1or2 (word, "<")
			end	
			
		else
			-- concat to make a longer word
			word = word .. c
			self.column = self.column + 1
		end
		
	end
	
	dbgHook ()
	print (input)
	
	return input
end


