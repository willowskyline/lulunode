require "lln_Instance"
require "lln_CycleFinder"
--	lulu
require "tokeniser"
require "ast"

lln_System = {}
lln_System.Metatable =
{__index = lln_System}

lln_System.List = {}


function lln_System.New (name, args, rets)
	local new = setmetatable (
	{	arguments	= args
	,	  returns	= rets
	
	,	argsdef
	  =	lln_Definition.NewStandard ("__lln_"..name.."_args"
	  ,	name
	  ,	{  }, args
	  , false
	  , "flow")
	
	,	retsdef
	  = lln_Definition.NewStandard ("__lln_"..name.."_rets"
	  ,	"return"
	  ,	rets, {  }
	  , false
	  , "flow")
	
	,	calldef
	  = lln_Definition.NewStandard ("__lln_"..name.."_call"
	  , name.." ()"
	  , args, rets
	  , false
	  , false)
	
	,	updatequeue = {}
	,	synclist = {}
	,	synctype = false
	}
	, lln_System.Metatable )
	
	new:AddInstance {d=new.argsdef, x=  0, y=20}
	new.source = new[1]
	
	new:AddInstance {d=new.retsdef, x=400, y=20}
	new.sink = new[2]
	
	lln_System.List [name] = new
	return new
end

function lln_System:AddInstance (a)
	local new = lln_Instance.New(a)
	table.insert (self, new)
	table.insert (self.updatequeue, new)
	if new.definition.sync then
		table.insert (self.synclist, new)
		self:SortSyncList()
	end
	return self
end

function lln_System:Draw (x,y)
	--for i,n in ipairs(self.updatequeue) do
	while #self.updatequeue > 0 do
		table.remove(self.updatequeue):UpdateCanvas()
	end
	for i,n0 in ipairs(self.synclist) do
		local col = n0.definition.sync
		local x0 = n0.x + 0.5*n0.width + 10 
		for j,n1 in ipairs(self.synclist) do
			if n0 ~= n1 then
				local x1 = n1.x + 0.5*n1.width + 10
				if math.abs (x0 - x1) <= 0.5*math.max(n0.width, n1.width) then
					col = "Warn"
					break
				end
			end
		end
		RSkyl.SetColor (col, 0.5)
		love.graphics.setLineWidth (6)
		love.graphics.line (x0,-999,x0,2000)
		RSkyl.SetColor (col, 1.0)
		love.graphics.setLineWidth (2)
		love.graphics.line (x0,-999,x0,2000)
	end
	RSkyl.SetColor("White")
	for i,n in ipairs(self) do
		n:DrawConnections(x,y)
	end
	love.graphics.setBlendMode ("alpha", "premultiplied")
	for i,n in ipairs(self) do
		n:Draw(x,y)
	end
	love.graphics.setBlendMode ("alpha")
	if lln_User.hand == "multi" then
		RSkyl.SetColor("White",0.5)
		love.graphics.polygon ('fill',
			lln_User.x0, lln_User.y0,
			lln_User.x1, lln_User.y0,
			lln_User.x1, lln_User.y1,
			lln_User.x0, lln_User.y1)
	end
end

function lln_System:Detect (x,y)
	for i,n in ipairs(self) do
		local res = n:Detect (x,y)
		if res then return res end
	end
end

function lln_System:RebuildSyncList ()
	self.synclist = {}
	for i,n in ipairs(self) do
		if n.definition.sync then
			table.insert (sync, n)
		end
	end
	return self:SortSyncList ()
end

function lln_System.SyncListSorter (n0,n1)
	local x0 = n0.x + 0.5*n0.width
	local x1 = n1.x + 0.5*n1.width
	if x0 == x1 then
		return n0.y < n1.y
	else
		return x0 < x1
	end
end

function lln_System:SortSyncList ()
	table.sort (self.synclist, lln_System.SyncListSorter)
	for i=1,#self.synclist do
		local n = self.synclist[i]
	end
end

function lln_System:CheckSyncViolations ()
	self:SortSyncList()
	for i,n in ipairs(self.synclist) do
		if lln_CycleFinder.CheckFor (n, RSkyl.TableSub(self.synclist, i+1)) then
			return true
		end
	end
end

function lln_System:UpdateSyncType ()
	local r = false
	local w = false
	
	for i,n in ipairs(self) do
		local s = n.definition.sync
		if s then
			    if s == "write" then w = true
			elseif s == "read"  then r = true
			elseif s == "wr" then
				w = true
				r = true
				break
			end
		end
	end
	
	self.synctype
		=  (w and r and "wr")
		or (w and "write")
		or (r and "read")
		or false
end

function lln_System:BuildAST ()
	self:SortSyncList ()
	print "called BuildAST"
	--	detect multi-use pinouts
	local refcount = {} -- Indexed by node -> {} -- Indexed by pin -> int
	--	set counts to zero                       -- var -> false ->> doesn't need intermediate
	for _,n in ipairs(self) do                   --     -> true  ->> needs intermediate
		local list = {var = false}               --     -> "str" ->> has intermediate of this name
		for _,p in pairs(n.definition.Component.PinOut) do
			list [p] = 0
		end
		refcount [n] = list
	end
	--	count references
	for _,n in ipairs(self) do
		for _,p in pairs(n.definition.Component.PinIn) do
			local r = n.ioref[p]
			if r.node then
				refcount[r.node][r.pin] = 1 + refcount[r.node][r.pin]
			end
		end
	end
	--	mark
	for _,n in ipairs(self) do
		for _,p in pairs(n.definition.Component.PinOut) do
			if refcount[n][p] > 1 then
				refcount[n].var = true
				print ("marked ".. n:GenPinName (p) .." @ ".. n.x ..",".. n.y)
			end
		end
	end
	
	
end


















