local LCSS = require ("LCSS")
local LLN  = require ("LLN")
local IO   = require ("IO")
local UI   = require ("UI")

function love.load ()
	LCSS.Load_core ()
	LLN .Load_core ()
	IO  .Load_core ()
	UI  .Load_core ()

	LCSS.Load_class ("Cut")

	local In =
	{	UI.Classes.Node.Input_new ()
	,	UI.Classes.Node.Input_new ()
	,	UI.Classes.Node.Input_new ()
	}
	local Out =
	{	LCSS.New ("Fill",
		{	padding = 1
		,	rgba = {0.5,0.5,0.5,1.0}
		})
	,	LCSS.New ("Fill",
		{	padding = 1
		,	rgba = {0.5,0.5,0.5,1.0}
		})
	}
	test_node = UI.New ("Node", "test", In, Out)
	test_lcss = LCSS.New ("Cut", {rgba={1,1,1,1}, padding = 1})
end

function Deep_print (value, i)
	i = i or ""
	print (i..tostring(value))
	if type(value) == "table" then
		local j = i.."    "
		i = i.."   "
		for k,v in pairs (value) do
			print (i..tostring(k))
			Deep_print (v, j)
		end
	end
end

local dbg = true
function love.draw ()
	local res = test_node:Draw (0,0,0,0,100,100,1)
	if dbg then
		Deep_print (test_node)
		print (res)
		dbg = false
	end
	test_lcss:Draw (600,0,600,0,200,600,1)
end
